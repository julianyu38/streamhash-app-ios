//
//  AFNetworkService.h
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceHandler.h"

//#define HOST @"https://cawordpressdemos.com"
#define API @""

//Service URL

#define HOST @"http://adminview.streamhash.com/" //@"http://default.startstreaming.info/"

//Methods

#define METHOD_REGISTER @"userApi/register"
#define METHOD_LOGIN @"userApi/login"
#define METHOD_FORGOT_PASSWORD @"userApi/forgotpassword"
#define METHOD_CHANGE_PASSWORD @"userApi/changePassword"
#define METHOD_GET_USER_PROFILE @"userApi/userDetails"
#define METHOD_PROFILE_UPDATE @"userApi/updateProfile"
#define METHOD_TOKEN_RENEW @"userApi/tokenRenew"
#define METHOD_PAYPAL @"userApi/paybypaypal"
#define METHOD_GET_CATEGORIES @"userApi/categories"
#define METHOD_GET_SUB_CATEGORIES @"userApi/subCategories"
#define METHOD_HOME @"userApi/home"
#define METHOD_GET_VIDEO @"userApi/categoryVideos"
#define METHOD_GET_SUB_CATEGORY @"userApi/subCategoryVideos"
#define METHOD_SEARCH_VIDEO @"userApi/searchVideo"
#define METHOD_SINGLE_VIDEO @"userApi/singleVideo"
#define METHOD_USER_RATING @"userApi/userRating"
#define METHOD_ADD_WISHLIST @"userApi/addWishlist"
#define METHOD_GET_WISHLIST @"userApi/getWishlist"
#define METHOD_DELETE_LIST @"userApi/deleteWishlist"
#define METHOD_ADD_HISTORY @"userApi/addHistory"
#define METHOD_GET_HISTROY @"userApi/getHistory"
#define METHOD_DELETE_HISTORY @"userApi/deleteHistory"
#define METHD_CLEAR_HISTORY @"userApi/deleteHistory"
#define METHOD_VIEW_ALL @"userApi/common"

#define METHOD_PRIVACY @"privacy"
#define METHOD_NOTIFICATION @"userApi/settings"
#define METHOD_DELETE @"userApi/deleteAccount"

#define METHOD_LIKE_VIDEO @"userApi/like_video"
#define METHOD_DISLIKE_VIDEO @"userApi/dis_like_video"
#define METHOD_REPORTSPAMLIST @"userApi/spam-reasons"
#define METHOD_ADDSPAM @"userApi/add_spam"
#define METHOD_SPAMVIDEOSLIST @"userApi/spam_videos"
#define METHOD_REMOVE_SPAM @"userApi/remove_spam"
#define METHOD_MANAGE_PROFILES @"userApi/active-profiles"
#define METHOD_ADD_PROFILE @"userApi/add-profile"

typedef void(^CompletionHandler)(NSDictionary *dict, NSURLResponse *response, NSError *error);

typedef void(^responseHandler)(NSDictionary *dict);
typedef void(^errorHandler)(NSError *error);

@interface AFNetworkService : NSObject

+ (AFNetworkService *)sharedInstance;

- (void)sendRequestForImageUpload:(NSString *)service urlParameters:(NSDictionary *)params method:(NSString *)method imageParam:(NSString *)imgParam imageData:(NSData *)imgData withCompletionHandler:(CompletionHandler)myCompletionHandler;

- (void)sendRequestGETandPOST:(NSString *)service postParams:(NSDictionary *)params method:(WebMethod)method withCompletionHandler:(responseHandler)myResponseHandler;

@end
