//
//  AddSubProfileVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 02/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "AddSubProfileVC.h"
#import "WebServiceHandler.h"
#import "AFNetworkService.h"
#import "ProgressIndicator.h"

@interface AddSubProfileVC ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImage *chosenImage;
    NSData *compressedImage;
    AFNetworkService *webService;
}

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIView *cardView;
@property (strong, nonatomic) IBOutlet UIView *profilePicBackView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UIView *deleteAccountView;

@end

@implementation AddSubProfileVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    webService = [[AFNetworkService alloc] init];
    
    self.cardView.layer.cornerRadius = 5.0;
    self.closeButton.layer.cornerRadius = self.closeButton.frame.size.width/2;
    self.saveButton.layer.cornerRadius = self.saveButton.frame.size.width/2;
    
    self.profilePicBackView.layer.cornerRadius = self.profilePicBackView.frame.size.width/2;
    self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width/2;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectProfilePicture:)];
    [self.profilePic addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.numberOfTapsRequired = 1;
    self.profilePic.userInteractionEnabled = YES;
    
    self.deleteAccountView.hidden = NO;
    
    UITapGestureRecognizer *deleteGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteViewTapped:)];
    [self.deleteAccountView addGestureRecognizer:deleteGesture];
    deleteGesture.numberOfTapsRequired = 1;
    self.deleteAccountView.userInteractionEnabled = YES;
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = YES;

    if ([_deleteORadd isEqualToString:@"add"]) {
        self.deleteAccountView.hidden = YES;
        
    }else{
        self.nameTF.text = [self.subProfileDict objectForKey:@"name"];
        
        NSURL *pictureURL = [NSURL URLWithString:[self.subProfileDict objectForKey:@"picture"]];
        NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
        UIImage *proImage = [UIImage imageWithData:imageData];
        self.profilePic.image = proImage;
        
        self.deleteAccountView.hidden = NO;
    }
    
}

- (IBAction)saveButtonTapped:(id)sender {
    
//    NSData *imgData = UIImagePNGRepresentation(chosenImage);
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject:self.nameTF.text forKey:@"name"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:ok];
    
    if ([self.nameTF.text isEqualToString:@""]) {
        
        alertController.message=@"Please Enter Name";
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }else{
        
        [webService sendRequestForImageUpload:METHOD_ADD_PROFILE urlParameters:postDataDict method:@"POST" imageParam:@"picture" imageData:compressedImage withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [self popup:@"Profile added successfully"];

                });
            }
        }];
    }
}

- (void)deleteViewTapped:(UITapGestureRecognizer *)sender{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject:[self.subProfileDict objectForKey:@"id"] forKey:@"sub_profile_id"];
    
    [self callService:METHOD_DELETE_SUBPROFILE params:postDataDict];
    
}

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params
    {
        __block NSError *error;
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];

        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
                
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];

                        [self popup:@"Done !"];
                    });
                }
                
            }];
        });

    }

- (void) popup:(NSString *)message{
    
    
    if (message == nil) {
        message = @"Done !";
    }
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

- (IBAction)closeButtonTapped:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectProfilePicture:(UITapGestureRecognizer *)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Stream View" message:@"Choose Image :" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
            
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sorry !" message:@"No Camera device found." preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        [self presentViewController:picker animated:YES completion:NULL];

    }];
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:camera];
    [alert addAction:gallery];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark ImagePicker delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    compressedImage = UIImageJPEGRepresentation(chosenImage, 0);
    self.profilePic.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
