//
//  CardCustomCell.h
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *cardBackView;
@property (strong, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *defaultCardButton;

- (IBAction)defaultCardTapped:(id)sender;
- (IBAction)deleteButtonTapped:(id)sender;

@end
