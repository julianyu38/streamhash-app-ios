//
//  CardsVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "CardsVC.h"
#import "CardCustomCell.h"
#import "SWRevealViewController.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import <Stripe/Stripe.h>
#import "AFNetworkService.h"
#import <AFNetworking.h>

@interface CardsVC () <UITableViewDelegate, UITableViewDataSource, STPAddCardViewControllerDelegate, STPPaymentCardTextFieldDelegate>
{
    NSMutableArray *cardsArray;
    NSIndexPath *lastSelected;
    NSUserDefaults *defaults;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end

@implementation CardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    STPPaymentCardTextField *STPtextField = [[STPPaymentCardTextField alloc] init];
    STPtextField.delegate = self;

    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor ],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    cardsArray = [NSMutableArray new];
    
    defaults = [[NSUserDefaults alloc] init];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    
    [self callService:METHOD_CARD_LIST params:postDataDict key:@"CardList"];
    
}
#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([dict objectForKey:@"success"]) {
                if ([key isEqualToString:@"DeleteCard"]) {
                    [self popup:@"Card deleted !"];

                }else if ([key isEqualToString:@"DefaultCard"]){
                    [self popup:@"Default card added !"];
                }
                else{
                    cardsArray = [dict objectForKey:@"data"];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([key isEqualToString:@"DefaultCard"]) {
                    // Don't reload Tableview.
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                }else{
                    [self.tableView reloadData];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];

                }
                
            });
        }];
    });
}
- (IBAction)addButtonTapped:(id)sender {
    
    // Present payment methods view controller
    STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
    addCardViewController.delegate = self;

    // Present add card view controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

#pragma mark STPAddCardViewControllerDelegate

- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController {
    
    // Dismiss add card view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController didCreateToken:(STPToken *)token completion:(STPErrorBlock)completion {
    
    defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    //New card number saved on STPaddCardPaymentVC
    [postDataDict setValue:[defaults valueForKey:@"NewCardNumber"] forKey:@"number"];
    [postDataDict setValue:token forKey:@"card_token"];
    
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%@",HOST,METHOD_ADD_CREDIT_CARD];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager POST:urlStr parameters:postDataDict progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSDictionary *responseDict = (NSDictionary *)responseDict;
        completion(nil);
        [self popup:[responseObject objectForKey:@"message"]];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        completion(error);
        
    }];
    
}


- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

- (IBAction)sideButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cardsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 165.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *cardDetails = [cardsArray objectAtIndex:indexPath.row];
    
    CardCustomCell *cardCell = (CardCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CardCustomCell"];
    
    if (!cardCell) {
        
        cardCell = [[CardCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardCustomCell"];
    }
    
    cardCell.cardNumberLabel.text = [NSString stringWithFormat:@"xxxx-xxxx-xxxx-%@",[cardDetails objectForKey:@"last_four"]];
    
    if ([[cardDetails objectForKey: @"is_default"] intValue] == 1) {
        cardCell.accessoryType = UITableViewCellAccessoryCheckmark;
        [cardCell setSelected:YES animated:YES];
        lastSelected = indexPath;
    }
    
    cardCell.deleteButton.tag = indexPath.row;
    [cardCell.deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cardCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setObject:[[cardsArray objectAtIndex:indexPath.row] objectForKey:@"card_id"] forKey:@"defaultCardID"];
    
    if (lastSelected == indexPath) return; // nothing to do
    
    // deselect old
    UITableViewCell *old = [self.tableView cellForRowAtIndexPath:lastSelected];
    old.accessoryType = UITableViewCellAccessoryNone;
    [old setSelected:FALSE animated:TRUE];
    
    // select new
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [cell setSelected:TRUE animated:TRUE];
    
    
    
    // keep track of the last selected cell
    lastSelected = indexPath;
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaults objectForKey:@"defaultCardID"] forKey:@"card_id"];
    
    [self callService:METHOD_ADD_DEFAULT_CARD params:postDataDict key:@"DefaultCard"];
}

- (void)deleteButtonTapped:(UIButton *)sender{
    
    NSDictionary *currentCardDetail = [cardsArray objectAtIndex:sender.tag];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[currentCardDetail objectForKey:@"card_id"] forKey:@"card_id"];
    [cardsArray removeObjectAtIndex:sender.tag];

    [self callService:METHOD_DELETE_CARD params:postDataDict key:@"DeleteCard"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
