//
//  ChangePasswordView.m
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "ChangePasswordView.h"
#import "ProgressIndicator.h"
#import "Webservice.h"
#import "AppDelegate.h"

@interface ChangePasswordView ()<WebServiceDelegate,UIGestureRecognizerDelegate>
{
    AppDelegate *appDeleagte;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    
    UITextField *currentTextView;
}

@end

@implementation ChangePasswordView

- (void)viewDidLoad {
    [super viewDidLoad];
    appDeleagte=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
    self.title=@"Change Password";
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardDidShow:)
//                                                 name:UIKeyboardDidShowNotification
//                                               object:self.view.window];
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardDidHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:self.view.window];
//    keyboardIsShown = NO;
    
        UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
        tapRecognizer1.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:tapRecognizer1];

    
    [self.txtCurrentPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtCurrentPassword.textColor=[UIColor whiteColor];
    
    [self.txtNewPwd setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtNewPwd.textColor=[UIColor whiteColor];
    
    [self.txtOldPwd setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtOldPwd.textColor=[UIColor whiteColor];

    
}
-(void)dismissKeyBoard
{
    [_txtCurrentPassword resignFirstResponder];
    [_txtNewPwd resignFirstResponder];
    [_txtOldPwd resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onChangePassword:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    // [self presentViewController:alertController animated:YES completion:nil];
    
    
     if ([_txtCurrentPassword.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Current Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([_txtNewPwd.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter New Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([_txtOldPwd.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Old Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([_txtNewPwd.text isEqualToString:_txtCurrentPassword.text])
    {
        alertController.message=@"The passwords entered does not match";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
        
    }
    

    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"password\":\"%@\",\"password_confirmation\":\"%@\",\"old_password\":\"%@\"}",strID,strToken,_txtNewPwd.text,_txtOldPwd.text,_txtCurrentPassword.text];
    
    [service executeWebserviceWithMethod:METHOD_CHANGE_PASSWORD withValues:strSend];

    
}

- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
            
            _txtCurrentPassword.text=@"";
            _txtNewPwd.text=@"";
            _txtOldPwd.text=@"";
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDeleagte.strAlertTitle message:appDeleagte.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDeleagte onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDeleagte.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
    
    
}
//#pragma mark -- Keyboard hide  and moving
//
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardDidShowNotification
//                                                  object:nil];
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}
//
//-(BOOL) textFieldShouldReturn:(UITextField *)textField{
//    
//    [textField resignFirstResponder];
//    return YES;
//}
//
//-(void) textFieldDidBeginEditing:(UITextField *)textField
//{
//    //    if (!textField.inputAccessoryView)
//    //    {
//    //        textField.inputAccessoryView = toolKeyboard;
//    //    }
//    currentTextView=textField;
//    
//    //    [self moveViewUp:YES toRect:textField.frame];
//    [self moveScrollView:textField.frame];
//}
//
//- (void)keyboardDidHide:(NSNotification *)n
//{
//    keyboardIsShown = NO;
//    [self.view removeGestureRecognizer:tapGesture];
//    
//    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
//}
//
//- (void)keyboardDidShow:(NSNotification *)n
//{
//    if (keyboardIsShown) {
//        return;
//    }
//    
//    keyboardIsShown = YES;
//    [scrollView addGestureRecognizer:tapGesture];
//}
//
//- (void) moveScrollView:(CGRect) rect
//{
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenHeight = screenRect.size.height;
//    
//    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
//    
//    // If active text field is hidden by keyboard, scroll it so it's visible
//    // Your application might not need or want this behavior.
//    CGRect aRect = scrollView.frame;
//    aRect.size.height -= (kbSize.height+55);
//    if (!CGRectContainsPoint(aRect, rect.origin) )
//    {
//        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
//        
//        if(screenHeight==480)
//            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
//        
//        if(scrollPoint.y>0)
//            [scrollView setContentOffset:scrollPoint animated:YES];
//    }
//}


@end
