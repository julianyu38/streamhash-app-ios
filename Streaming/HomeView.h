//
//  HomeView.h
//  Streaming
//
//  Created by Ramesh on 19/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeView : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *SideBarButton;
@end
