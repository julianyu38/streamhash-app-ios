//
//  HomeView.m
//  Streaming
//
//  Created by Ramesh on 19/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "HomeView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import "viewAllViewController.h"
#import "SearchBarVC.h"
#import "SearchBarVC.h"


@interface HomeView ()<WebServiceDelegate,UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating,UIScrollViewDelegate>
{
    AppDelegate *appDelegate;
    UIScrollView *scrollView;
    NSArray *arrList;
      NSArray *arrBanner;
    
    NSArray *arrViewAll;
    
    NSDictionary *dictSendVal;
    NSString *strAd_VideoID;
    UIScrollView *imgScrlView;
    UIPageControl *pageControl;
    int nBannerCount;
    
    NSTimer *nTime;
    
    NSString *strViewAllTitle;
    
    NSString *strSendKey,*strTotalCount,*strIdenty;
}
//Fetch result controller
@property (nonatomic, strong) UISearchController *searchController;



@end

@implementation HomeView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"streamview.png"]];
    
    UIImage *img = [UIImage imageNamed:@"streamview.png"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 75, 25)];
    [imgView setImage:img];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    // setContent mode aspect fit
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    strIdenty=@"home";
   
//    self.title=@"Home";
    nBannerCount=0;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strID,strToken];
    
    [service executeWebserviceWithMethod:METHOD_HOME withValues:strSend];
    
    
    
    // Menus
    Webservice *service1=[[Webservice alloc]init];
    service1.tag=10;
    service1.delegate=self;
    
    NSString *strSend1=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strID,strToken];
    [service1 executeWebserviceWithMethod:METHOD_GET_CATEGORIES withValues:strSend1];
}

-(void)viewWillAppear:(BOOL)animated
{
//    [[UIDevice currentDevice] setValue:
//     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
//                                forKey:@"orientation"];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO]; // Edited
//     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
    
    
//  self.navigationController.navigationBar.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
}
//- (IBAction)searchButtonTapped:(id)sender {
//    SearchBarVC *searchVideos = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchBarVC"];
//    [self.navigationController pushViewController:searchVideos animated:YES];
//}

-(void)viewanimation
{
    if (nBannerCount<arrBanner.count)
    {
        nBannerCount++;
        int pageNumber = nBannerCount;
        
        CGRect frame = imgScrlView.frame;
        frame.origin.x = frame.size.width*pageNumber;
        frame.origin.y=0;
        
        [imgScrlView scrollRectToVisible:frame animated:YES];
    }
    else
    {
        nBannerCount=0;
        int pageNumber = nBannerCount;
        
        CGRect frame = imgScrlView.frame;
        frame.origin.x = frame.size.width*pageNumber;
        frame.origin.y=0;
        
        [imgScrlView scrollRectToVisible:frame animated:YES];

    }
}
- (IBAction)searchButtonTapped:(id)sender {
    
    SearchBarVC *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchBarVC"];
    [self.navigationController pushViewController:search animated:YES];
    
}
-(void)onPageLoad
{
    [scrollView removeFromSuperview];
    
    int n=self.view.frame.size.width/3-10;
    
    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.showsHorizontalScrollIndicator=NO;
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    
    if (arrBanner.count!=0)
    {
        int pageCount=arrBanner.count;
        
        int xPos=5;
        
        imgScrlView=[[UIScrollView alloc]initWithFrame:CGRectMake(xPos, 5, self.view.frame.size.width-10, self.view.frame.size.height/3)];
        imgScrlView.backgroundColor=[UIColor clearColor];
        imgScrlView.pagingEnabled=YES;
        imgScrlView.delegate=self;
        imgScrlView.contentSize=CGSizeMake(pageCount * imgScrlView.bounds.size.width , imgScrlView.bounds.size.height);
        [imgScrlView setShowsHorizontalScrollIndicator:NO];
        [imgScrlView setShowsVerticalScrollIndicator:NO];
        [scrollView addSubview:imgScrlView];
        
        CGRect viewSize;//=imgScrlView.bounds;
        for (int i=0; i<arrBanner.count; i++)
        {
            if (i==0)
                viewSize=imgScrlView.bounds;
            else
                viewSize=CGRectOffset(viewSize, imgScrlView.bounds.size.width, 0);
            
            NSDictionary *dictLoc= [arrBanner objectAtIndex:i];
            
          //  NSString *strImageURL;//=[dictLocal valueForKey:@"EventImageURL"] ;
            UIImageView *imgView=[[UIImageView alloc] initWithFrame:viewSize];
            [imgView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            imgView.clipsToBounds = YES;
           [imgScrlView addSubview:imgView];
            
            UIButton *btnClick=[[UIButton alloc]initWithFrame:viewSize];
            btnClick.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
            // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
            [imgScrlView addSubview:btnClick];
            
            //Single tap
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
            tapGesture.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
            tapGesture.numberOfTapsRequired = 1;
            tapGesture.numberOfTouchesRequired = 1;
            [btnClick addGestureRecognizer:tapGesture];
            
//
//            UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0,self.view.frame.size.width/3-15,130)];
//            viewContent.backgroundColor = [UIColor whiteColor];
//            viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
//            viewContent.layer.shadowOpacity = 0.7f;
//            viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
//            viewContent.layer.shadowRadius = 1.0f;
//            viewContent.layer.cornerRadius = 2.0f;
//            viewContent.tag=123;
//            [imgScrlView addSubview:viewContent];
            
            
            
        }
        pageControl = [[UIPageControl alloc] init];
        pageControl.frame = CGRectMake(imgScrlView.frame.size.width/2-70,self.view.frame.size.height/3-40,150,60);
        pageControl.numberOfPages = pageCount;
        pageControl.currentPage = 0;
       // [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
        [scrollView addSubview:pageControl];
        
     nTime=[NSTimer  scheduledTimerWithTimeInterval:05.0 target:self selector:@selector(viewanimation) userInfo:nil repeats:YES];
    }
    
    
    
   int  yPos=self.view.frame.size.height/3+20;
    for (int nCount=0; nCount<arrList.count; nCount++)
    {
        int xPos=5;
        
        NSDictionary *dictLocal=[arrList objectAtIndex:nCount];
        NSArray *arrLocal=[dictLocal valueForKey:@"list"];
        if (arrLocal.count!=0)
        {
            UILabel *lblNameTile=[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width-70, 25)];
            lblNameTile.text=[[dictLocal valueForKey:@"name"] uppercaseString];
            [lblNameTile setFont:[UIFont boldSystemFontOfSize:15]];
            lblNameTile.textAlignment=NSTextAlignmentLeft;
            lblNameTile.textColor=[UIColor lightGrayColor];
            [scrollView addSubview:lblNameTile];
            
            UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width
                                                                        -55, yPos, 50, 20)];
            [btnMore setTitle:[dictLocal valueForKey:@"key"] forState:UIControlStateFocused];
            [btnMore setTitle:[dictLocal valueForKey:@"name"] forState:UIControlStateHighlighted];
//            [btnMore setTitle:@"See All >" forState:UIControlStateNormal];
            [btnMore setImage:[UIImage imageNamed:@"arrow_right_white.png"] forState:UIControlStateNormal];
            [btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
          //  [btnMore setTitleColor:[UIColor colorWithRed:218.0f/255.0f green:106.0f/255.0f blue:44.0f/255.0f alpha:1] forState:UIControlStateNormal];
            [btnMore.titleLabel setFont:[UIFont systemFontOfSize:13]];
            btnMore.contentHorizontalAlignment=NSTextAlignmentRight;
            // btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
            [btnMore addTarget:self action:@selector(onViewAll:) forControlEvents:UIControlEventTouchDown];
           // [btnMore setBackgroundImage:[UIImage imageNamed:@"view_more.png"] forState:UIControlStateNormal];
            [scrollView addSubview:btnMore];
            
            yPos+=30;
            
            UIScrollView *scrollViewSubView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width,150)];
            scrollViewSubView.showsHorizontalScrollIndicator=NO;
            scrollViewSubView.showsVerticalScrollIndicator=NO;
            [scrollView addSubview:scrollViewSubView];
            
            for (int i=0; i<arrLocal.count; i++)
            {
                NSDictionary *dictLoc=[arrLocal objectAtIndex:i];
                
                UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0,115,140)];
                viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
                //viewContent.opaque=NO;
               // viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
                viewContent.layer.shadowOpacity = 1.0f;
                viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
                viewContent.layer.shadowRadius = 1.0f;
                viewContent.layer.cornerRadius = 2.0f;
               // viewContent.alpha=0.5;
                viewContent.tag=123;
                [scrollViewSubView addSubview:viewContent];
                
                UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,140)];
                imageView.layer.cornerRadius = 2.0f;
                [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                [viewContent addSubview:imageView];
                
                
                UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
                imgPlay.image=[UIImage imageNamed:@"play.png"];
                //imgPlay.alpha=0.6;
//                [viewContent addSubview:imgPlay];
                
                UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
                btnClick.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
               // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
                [viewContent addSubview:btnClick];
                
                //Single tap
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
                tapGesture.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
                tapGesture.numberOfTapsRequired = 1;
                tapGesture.numberOfTouchesRequired = 1;
                [btnClick addGestureRecognizer:tapGesture];
                
                //Long press
//                UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClear:)];
//                longPress.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//                longPress.minimumPressDuration = 1.5;
//                [btnClick addGestureRecognizer:longPress];
                
                
                UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 115, viewContent.frame.size.width-10, 20)];
                
                lblTitle.text=[dictLoc valueForKey:@"title"];
                [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
                lblTitle.textColor=[UIColor whiteColor];
                lblTitle.lineBreakMode = NSLineBreakByClipping;
                //lblTitle.minimumFontSize = 0;
//                [viewContent addSubview:lblTitle];
                
//                UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 123, viewContent.frame.size.width, 20)];
//                lblDate.text=[dictLoc valueForKey:@"publish_time"];
//                [lblDate setFont:[UIFont systemFontOfSize:12]];
//                lblDate.textColor=[UIColor grayColor];
//                [viewContent addSubview:lblDate];
                
//                UILabel *lblCategoryName=[[UILabel alloc]initWithFrame:CGRectMake(5, 130, viewContent.frame.size.width, 20)];
//                lblCategoryName.text=[dictLoc valueForKey:@"category_name"];
//                [lblCategoryName setFont:[UIFont boldSystemFontOfSize:13]];
//                lblCategoryName.textColor=[UIColor colorWithRed:230.0f/255.0f green:130.0f/255.0f blue:62.0f/255.0f alpha:1];
//                [viewContent addSubview:lblCategoryName];
               
//                UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(viewContent.frame.size.width-25, viewContent.frame.size.height-25, 25, 25)];
//                btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//                [btnMore addTarget:self action:@selector(onSingleClear:) forControlEvents:UIControlEventTouchDown];
//                [btnMore setBackgroundImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
//                [viewContent addSubview:btnMore];

                xPos+=115+5;
                
                
//                if ((i+1)==arrLocal.count)
//                {
//                    UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0,self.view.frame.size.width/3-15,150)];
//                    viewContent.backgroundColor = [UIColor whiteColor];
//                    viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
//                    viewContent.layer.shadowOpacity = 0.7f;
//                    viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
//                    viewContent.layer.shadowRadius = 1.0f;
//                    viewContent.layer.cornerRadius = 2.0f;
//                    viewContent.tag=123;
//                    [scrollViewSubView addSubview:viewContent];
//                    
//                    UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(3, 25, viewContent.frame.size.width, viewContent.frame.size.width)];
//                    [btnMore setTitle:[dictLocal valueForKey:@"key"] forState:UIControlStateNormal];
//                    [btnMore setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//                   // btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//                    [btnMore addTarget:self action:@selector(onViewAll:) forControlEvents:UIControlEventTouchDown];
//                    [btnMore setBackgroundImage:[UIImage imageNamed:@"view_more.png"] forState:UIControlStateNormal];
//                    [viewContent addSubview:btnMore];
//                    
//                    xPos+=(self.view.frame.size.width/3-15)+13;
//
//                }
            }
            
            scrollViewSubView.contentSize=CGSizeMake(xPos, 150);
            yPos+=160;
        }
        
     }
    
    
     scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+50);
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView1{
    
    CGFloat viewWidth = scrollView1.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    int pageNumber = floor((scrollView1.contentOffset.x - viewWidth/50) / viewWidth) +1;
    
    pageControl.currentPage=pageNumber;
    
}

- (void)pageChanged
{
    
    int pageNumber = pageControl.currentPage;
    
    CGRect frame = imgScrlView.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [imgScrlView scrollRectToVisible:frame animated:YES];
}

-(IBAction)onViewAll:(id)sender
{
    UIButton *btn=(UIButton *) sender;
    

    strViewAllTitle=[btn titleForState:UIControlStateHighlighted];
    
   // NSString *strVal=[btn titleForState:UIControlStateFocused];
    
    strSendKey=[btn titleForState:UIControlStateFocused];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=4;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"key\":\"%@\",\"skip\":\"%@\"}",strID,strToken,[btn titleForState:UIControlStateFocused],@"0"];
    
    [service executeWebserviceWithMethod:METHOD_VIEW_ALL withValues:strSend];
    
    
}

-(IBAction)onSingleClear:(id)sender
{
    UIButton *btn=(UIButton *) sender;
    
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=3;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
    
    
}

//-(IBAction)onBannerClick:(id)sender
//{
//    UIButton *btn=(UIButton *) sender;
//    NSString *strVideoID=[NSString stringWithFormat:@"%ld",btn.tag];
//    
//    strAd_VideoID=strVideoID;
//    [self performSegueWithIdentifier:@"singleVideo" sender:self];
//    
//}

-(IBAction)onBtnClick:(id)sender
{
     UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    //UIButton *btn=(UIButton *) sender;
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    
    strAd_VideoID=strVideoID;
     [self performSegueWithIdentifier:@"singleVideo" sender:self];
    
    
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.delegate=self;
//    service.tag=2;
//    
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    NSString *strID=[defaluts valueForKey:@"id"];
//    NSString *strToken=[defaluts valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
//    
//    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrList=[dictResponse valueForKey:@"data"];
            arrBanner=[[dictResponse valueForKey:@"banner"] objectForKey:@"list"];
            [self onPageLoad];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
//    else if (webservice.tag==2)
//    {
//        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
//        {
//            dictSendVal=dictResponse;
//            [self performSegueWithIdentifier:@"singleVideo" sender:self];
//
//            
//        }
//        else
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }
//    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrViewAll=[dictResponse valueForKey:@"data"];
            strTotalCount=[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"total"]];
            
            if (arrViewAll.count !=0)
            {
                [self performSegueWithIdentifier:@"view_all" sender:self];
                
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"No more details" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];

            }
//            dictSendVal=dictResponse;
//            [self performSegueWithIdentifier:@"singleVideo" sender:self];
            
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    }
    if (webservice.tag==10)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            appDelegate.arrMenuCategoryList=[dictResponse valueForKey:@"categories"];
//            arrGetCategory=[dictResponse valueForKey:@"categories"];
//            [self onCreateMenuItems];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse objectForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
        
    }
    
}
-(IBAction)onSearchBar:(id)sender
{
    
//    //UISearchController
//    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
//    self.searchController.searchResultsUpdater = self;
//    self.searchController.dimsBackgroundDuringPresentation = NO;
//    self.searchController.searchBar.delegate = self;
// //   self.tableView.tableHeaderView = self.searchController.searchBar;
//    self.searchController.searchBar.scopeButtonTitles = @[];
//    self.definesPresentationContext = YES;
//    [self.searchController.searchBar sizeToFit];
//    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
//        singleVideo.dictGetValue=dictSendVal;
        singleVideo.strVideoID=strAd_VideoID;
     }
    else if ([[segue identifier] isEqualToString:@"view_all"])
    {
        viewAllViewController *view=[segue destinationViewController];
        view.arrList=arrViewAll;
        view.strTitleName=strViewAllTitle;
        view.strKey=strSendKey;
        view.strTotalCount=strTotalCount;
        view.strIdenty=strIdenty;
    }
    
}


@end
