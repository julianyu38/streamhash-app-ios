//
//  InvoiceVC.h
//  Streaming
//
//  Created by Aravinth Ramesh on 06/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *payButton;

@property (strong, nonatomic) NSDictionary *subscriptionDict;
@property (strong, nonatomic) UIViewController *backVC;


@end
