//
//  InvoiceVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 06/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "InvoiceVC.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"

@interface InvoiceVC ()
{
    NSUserDefaults *defaults;
}
@property (strong, nonatomic) IBOutlet UIView *invoiceBackView;

@end

@implementation InvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    UIBlurEffect *blurBG = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *visualEffect = [[UIVisualEffectView alloc] initWithEffect:blurBG];
//    visualEffect.frame = self.view.bounds;
//    [self.view addSubview:visualEffect];
    
    

}

- (void)viewWillAppear:(BOOL)animated{
//    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//        self.view.backgroundColor = [UIColor clearColor];
//
//        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//        //always fill the view
//        blurEffectView.frame = self.backVC.view.bounds;
//        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//
//        [self.backVC.view addSubview: blurEffectView];
//        //if you have more UIViews, use an insertSubview API to place it where needed
//    } else {
//        self.view.backgroundColor = [UIColor blackColor];
//    }
}
- (IBAction)closeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)payButtonTapped:(id)sender {
    
    defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:[self.subscriptionDict objectForKey:@"subscription_id"] forKey:@"subscription_id"];
    
    [self callService:METHOD_STRIPE_PAY_PLAN params:postDataDict key:@"CardList"];
    
}

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([dict objectForKey:@"success"]) {
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[dict objectForKey:@"message"] message:[NSString stringWithFormat:@"Txn ID: %@",[dict objectForKey:@"payment_id"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }];
                
                [alert addAction:okAction];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                
            });
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
