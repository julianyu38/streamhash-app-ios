//
//  LoginView.h
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginView : UIViewController<WebServiceDelegate, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate>
{
    IBOutlet UIScrollView *scrollView;
     UITextField *txtEmail;
     UITextField *txtPassword;
}

//@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
//@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

- (IBAction)onForgotPWD:(id)sender;
- (IBAction)onSignup:(id)sender;
- (IBAction)onLogin:(id)sender;
-(IBAction)onGoogle:(id)sender;
-(IBAction)onFacebook:(id)sender;
@end
