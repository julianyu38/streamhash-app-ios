//
//  ManageProfileCell.h
//  Streaming
//
//  Created by Aravinth Ramesh on 02/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageProfileCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *profilePicBackView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@end
