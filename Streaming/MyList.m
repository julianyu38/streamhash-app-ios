//
//  MyList.m
//  Streaming
//
//  Created by Ramesh on 24/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "MyList.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import "WebServiceHandler.h"
#import "SlideMenuView.h"

@interface MyList ()<WebServiceDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    AppDelegate *appDelegate;
    NSMutableArray *arrList;
    UIScrollView *scrollView;
    NSDictionary *dictSendVal;
    NSString *strAD_VideoID;
    NSString *strClearID;
    NSDictionary *dictLocal;
//    UILabel *lblTitle;
//    UIButton *btnClearAll;
    
    NSMutableArray *arrAllList;
    
    UIRefreshControl *refreshControl;
    
    int nSendSkipVal;
    
    UICollectionView *_collectionView;
    int nTotalCount;
}




@end

@implementation MyList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    self.title=appDelegate.strListORHistroyORSpam;
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor ],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    arrList=[[NSMutableArray alloc] init];
    nSendSkipVal=12;
    


    
    
//    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    scrollView.showsHorizontalScrollIndicator=NO;
//    scrollView.showsVerticalScrollIndicator=NO;
//    [self.view addSubview:scrollView];

    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-110) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
    
    //    refreshControl = [[UIRefreshControl alloc] init];
    //    refreshControl.tintColor = [UIColor whiteColor];
    //    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    //    [_collectionView addSubview:refreshControl];
    
    _collectionView.alwaysBounceVertical=YES;
    _collectionView.showsVerticalScrollIndicator=NO;
    _collectionView.showsHorizontalScrollIndicator=NO;
    
    refreshControl = [UIRefreshControl new];
    
    [refreshControl addTarget:self action:@selector(Refresh:) forControlEvents:UIControlEventValueChanged];
    _collectionView.bottomRefreshControl = refreshControl;
    

    
    [self onCallListWebService];
    
        
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
}
-(void)onCallListWebService

{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"skip\":\"%@\"}",strID,strToken,@"0"];
    
    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"]){
        
        [service executeWebserviceWithMethod:METHOD_GET_WISHLIST withValues:strSend];
        
    }else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        
        NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
        [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
        [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
        [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
        
        [self myWebservice:postDataDict];
        
    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"]){
        
        [service executeWebserviceWithMethod:METHOD_GET_HISTROY withValues:strSend];

    }
    
}

- (void)myWebservice:(NSDictionary *)params{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[WebServiceHandler sharedInstance] sendRequest:METHOD_SPAMVIDEOSLIST isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            [arrList addObjectsFromArray:[dict objectForKey:@"data"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
                [self labelAndClearUpdate];
                
            });
        }];
    });
 
    
}

- (void) labelAndClearUpdate{
    UILabel *lblTitle;
    UIButton *btnClearAll;
    
    if (arrList.count!=0)
    {
        lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
        
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            lblTitle.text=@"My List";
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
            lblTitle.text = @"History";
        else
            lblTitle.text=@"Spam";
        
        [lblTitle setFont:[UIFont boldSystemFontOfSize:15]];
        lblTitle.textColor=[UIColor whiteColor];
        [self.view addSubview:lblTitle];
        
        btnClearAll=[UIButton buttonWithType:UIButtonTypeCustom];
        btnClearAll.frame=CGRectMake(self.view.frame.size.width-110, 20,100, 30);
        [btnClearAll setTitle:@"Clear all" forState:UIControlStateNormal];
        [btnClearAll.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [btnClearAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnClearAll.backgroundColor=[UIColor colorWithRed:94.0f/255.0f green:94.0f/255.0f blue:100.0f/255.0f alpha:1];
        [btnClearAll addTarget:self action:@selector(onClearAll:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:btnClearAll];
        
        [_collectionView reloadData];
    }
    else
    {
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [self.view addSubview:imgeView];
        
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops!  List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [self.view addSubview:lblName];
    }
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

#pragma mark -- collectionview data source and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    //cell.backgroundColor=[UIColor greenColor];
    
    dictLocal=[arrList objectAtIndex:indexPath.row];
    
    [[cell viewWithTag:123] removeFromSuperview];   // REMOVING PREVIOUS CONTENT VIEW
    
    UIView *viewContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
    viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewContent.layer.shadowOpacity = 0.7f;
    viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    viewContent.layer.shadowRadius = 1.0f;
    viewContent.layer.cornerRadius = 2.0f;
    viewContent.tag=123;
    [cell addSubview:viewContent];
    
//    if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
//        cell.tag=[[dictLocal valueForKey:@"wishlist_id"]intValue];
//    else
//        cell.tag=[[dictLocal valueForKey:@"history_id"]intValue];
//    
    
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,110)];
    imageView.layer.cornerRadius = 2.0f;
    [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [viewContent addSubview:imageView];
    
    UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
    imgPlay.image=[UIImage imageNamed:@"play.png"];
    //imgPlay.alpha=0.6;
    [viewContent addSubview:imgPlay];
    
    UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
    btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
    [viewContent addSubview:btnClick];
    
    
    //Single tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
    tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [btnClick addGestureRecognizer:tapGesture];
    
    //Long press
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
                if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
                    cell.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
                else
                    cell.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
  //  longPress.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    longPress.minimumPressDuration = 1;
    [cell addGestureRecognizer:longPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 113, viewContent.frame.size.width-10, 20)];
    
    lblTitle.text=[dictLocal valueForKey:@"title"];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.lineBreakMode = NSLineBreakByClipping;
    [viewContent addSubview:lblTitle];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3-10, 140);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)Refresh:(UIRefreshControl *)refreshControl1
{
    
    if (nTotalCount>nSendSkipVal)
    {
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=4;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"skip\":\"%d\"}",strID,strToken,nSendSkipVal];
        
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            [service executeWebserviceWithMethod:METHOD_GET_WISHLIST withValues:strSend];
        else
            [service executeWebserviceWithMethod:METHOD_GET_HISTROY withValues:strSend];
        
        nSendSkipVal+=12;
    }
    else
    {
        [refreshControl1 endRefreshing];
        
    }
}

//#pragma mark -- Scrollview Delegate
//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
//{
//    
//}
//
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView1
//{
//    
//    //  [refreshControl beginRefreshing];
//    
//    float scrollViewHeight = scrollView1.frame.size.height;
//    float scrollContentSizeHeight = scrollView1.contentSize.height;
//    float scrollOffset = scrollView1.contentOffset.y;
//    
//    if (scrollOffset == 0)
//    {
//        // then we are at the top
//    }
//    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
//    {
//        // then we are at the end
//        [self callWebService];
//        
//        //  [refreshControl beginRefreshing];
//        //   [arrAllList addObjectsFromArray:arrList];
//        //arrAllList=[NSMutableArray arrayWithArray:arrList];
//        //   [self onPageLoad];
//    }
//    
//}
//-(void)callWebService
//{
//    
//    if (nTotalCount>nSendSkipVal)
//    {
//        Webservice *service=[[Webservice alloc]init];
//        service.delegate=self;
//        service.tag=4;
//        
//        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//        NSString *strID=[defaluts valueForKey:@"id"];
//        NSString *strToken=[defaluts valueForKey:@"token"];
//        
//         NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"skip\":\"%d\"}",strID,strToken,nSendSkipVal];
//        
//        if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
//            [service executeWebserviceWithMethod:METHOD_GET_WISHLIST withValues:strSend];
//        else
//            [service executeWebserviceWithMethod:METHOD_GET_HISTROY withValues:strSend];
//        
//        nSendSkipVal+=12;
//    }
//    
//    
//    
//    
//}
/*
-(void)onPageLoad
{
   /// [scrollView removeFromSuperview];
 
    
    for (UIView *v in scrollView.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
    int yPos=20;
    int xPos=10;
    int nVal=1;
    
    
    if (arrList.count!=0)
    {
        UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, yPos, 100, 20)];
        
        if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
            lblTitle.text=@"My List";
        else
            lblTitle.text=@"History";
        
        [lblTitle setFont:[UIFont boldSystemFontOfSize:15]];
        lblTitle.textColor=[UIColor whiteColor];
        [scrollView addSubview:lblTitle];
        
        UIButton *btnClearAll=[UIButton buttonWithType:UIButtonTypeCustom];
        btnClearAll.frame=CGRectMake(self.view.frame.size.width-110, yPos,100, 30);
        [btnClearAll setTitle:@"Clear all" forState:UIControlStateNormal];
        [btnClearAll.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [btnClearAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnClearAll.backgroundColor=[UIColor colorWithRed:94.0f/255.0f green:94.0f/255.0f blue:100.0f/255.0f alpha:1];
        [btnClearAll addTarget:self action:@selector(onClearAll:) forControlEvents:UIControlEventTouchDown];
        [scrollView addSubview:btnClearAll];
        
        yPos+=40;
        
        
        for (int nCount=1; nCount<=arrList.count; nCount++)
        {
            NSDictionary *dictLocal=[arrList objectAtIndex:nCount-1];
            
            UIView *viewContent;
            
            if (nVal==1)
            {
                xPos=5;
                nVal=2;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
            }
            else if (nVal==2)
            {
                xPos+=self.view.frame.size.width/3-3;
                nVal=3;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
            }
            else if (nVal==3)
            {
                xPos+=self.view.frame.size.width/3-3;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
                xPos=13;
                yPos+=160;
                nVal=1;
            }
            
            
            viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
            viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
            viewContent.layer.shadowOpacity = 0.7f;
            viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
            viewContent.layer.shadowRadius = 1.0f;
            viewContent.layer.cornerRadius = 2.0f;
            if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
                viewContent.tag=[[dictLocal valueForKey:@"wishlist_id"]intValue];
            else
                viewContent.tag=[[dictLocal valueForKey:@"history_id"]intValue];
            [scrollView addSubview:viewContent];
            
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,110)];
            imageView.layer.cornerRadius = 2.0f;
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            [viewContent addSubview:imageView];
            
            UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
            imgPlay.image=[UIImage imageNamed:@"play.png"];
            //imgPlay.alpha=0.6;
            [viewContent addSubview:imgPlay];
            
            UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
            btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
           // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
            [viewContent addSubview:btnClick];
            
            
            //Single tap
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
            tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
            tapGesture.numberOfTapsRequired = 1;
            tapGesture.numberOfTouchesRequired = 1;
            [btnClick addGestureRecognizer:tapGesture];
            
            //Long press
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
//            if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
//                longPress.view.tag=[[dictLocal valueForKey:@"wishlist_id"]intValue];
//            else
//                longPress.view.tag=[[dictLocal valueForKey:@"history_id"]intValue];
            //longPress.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
            longPress.minimumPressDuration = 1.5;
            [viewContent addGestureRecognizer:longPress];
            
            
            
            
            UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 113, viewContent.frame.size.width-10, 20)];
            
            lblTitle.text=[dictLocal valueForKey:@"title"];
            [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
            lblTitle.textColor=[UIColor whiteColor];
            lblTitle.lineBreakMode = NSLineBreakByClipping;
            [viewContent addSubview:lblTitle];
            
//            UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 123, viewContent.frame.size.width-10, 20)];
//            lblDate.text=[dictLocal valueForKey:@"publish_time"];
//            [lblDate setFont:[UIFont systemFontOfSize:12]];
//            lblDate.textColor=[UIColor grayColor];
//            [viewContent addSubview:lblDate];
            
//            UILabel *lblCategoryName=[[UILabel alloc]initWithFrame:CGRectMake(5, 130, viewContent.frame.size.width, 20)];
//            lblCategoryName.text=[dictLocal valueForKey:@"category_name"];
//            [lblCategoryName setFont:[UIFont boldSystemFontOfSize:13]];
//            lblCategoryName.textColor=[UIColor colorWithRed:230.0f/255.0f green:130.0f/255.0f blue:62.0f/255.0f alpha:1];
//            [viewContent addSubview:lblCategoryName];
            
            
//            UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(viewContent.frame.size.width-25, viewContent.frame.size.height-25, 25, 25)];
//             if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
//                 btnMore.tag=[[dictLocal valueForKey:@"wishlist_id"]intValue];
//             else
//                 btnMore.tag=[[dictLocal valueForKey:@"history_id"]intValue];
//            [btnMore addTarget:self action:@selector(onSingleClear:) forControlEvents:UIControlEventTouchDown];
//            [btnMore setBackgroundImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
//            [viewContent addSubview:btnMore];
            
        }

    }
    else
    {
        yPos=150;
        
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, yPos, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [scrollView addSubview:imgeView];
        
        yPos+=110;
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops!  List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [scrollView addSubview:lblName];
        
        yPos+=35;
    }
    
      scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+200);

}*/
-(IBAction)onBtnClick:(id)sender
{

        UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
        
        // UIButton *btn=(UIButton *) sender;
        NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        strAD_VideoID =  strVideoID;
        [self performSegueWithIdentifier:@"singleVideo" sender:self];
    }
    


    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.delegate=self;
//    service.tag=3;
//    
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    NSString *strID=[defaluts valueForKey:@"id"];
//    NSString *strToken=[defaluts valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
//    
//    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];

-(IBAction)onClearAll:(id)sender
{
//    SlideMenuView *slideMenuViewController = (SWRevealViewController*)self.revealViewController.rearViewController;
//    [slideMenuViewController performSegueWithIdentifier:@"Home" sender:self];

    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];

    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=2;

    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];

    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
    {
        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];

        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];

        [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
        [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
        [paramsDict setObject:@"" forKey:@"wishlist_id"];
        [paramsDict setObject:@"1" forKey:@"status"];  // Clear all

        [self clearAllService:paramsDict andService:METHOD_DELETE_LIST];

//        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"wishlist_id\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"",@"1"];
//        [service executeWebserviceWithMethod:METHOD_DELETE_LIST withValues:strSend];
    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
    {

        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];

        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];

        [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
        [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
        [paramsDict setObject:@"" forKey:@"history_id"];
        [paramsDict setObject:@"1" forKey:@"status"];  // Clear all

        [self clearAllService:paramsDict andService:METHD_CLEAR_HISTORY];

//         NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"history_id\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"",@"1"];
//        [service executeWebserviceWithMethod:METHD_CLEAR_HISTORY withValues:strSend];
    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"])
    {

        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];

        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];

        [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
        [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
        [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
        [paramsDict setObject:@"1" forKey:@"status"];  // Clear all

        [self clearAllService:paramsDict andService:METHOD_REMOVE_SPAM];

//        NSData *postData = [NSJSONSerialization dataWithJSONObject:paramsDict options:0 error:&error];
//
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//            [[WebServiceHandler sharedInstance] sendRequest:METHOD_REMOVE_SPAM isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
//
//                if ([[dict objectForKey:@"success"] intValue] == 1) {
//
//                    [arrList removeAllObjects];
//                    [self popup:[dict objectForKey:@"message"]];
//
//                }
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    [_collectionView reloadData];
//                    [self labelAndClearUpdate];
//
//                });
//
//            }];
//
//
//        });
//
//        [[ProgressIndicator sharedInstance]hideProgressIndicator];

    }

}

- (void)clearAllService:(NSMutableDictionary *)params andService:(NSString *)service{
    __block NSError *error;

    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                
                [arrList removeAllObjects];
                [self popup:[dict objectForKey:@"message"]];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_collectionView reloadData];
                [self labelAndClearUpdate];
                
            });
            
        }];
        
        
    });
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

}

- (void) popup:(NSString *)message{
    

    if (message == nil) {
        message = @"Done !";
    }
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
//        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
//        imgeView.image=[UIImage imageNamed:@"oops.png"];
//        [self.view addSubview:imgeView];
        
    });
}
- (void)handleLongPress:(UILongPressGestureRecognizer*)sender

{
    //NSLog(@"%ld", sender.view.tag);
    
   // UIButton *btn=(UIButton *) sender;
    
   UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    strClearID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service = [[Webservice alloc]init];
        service.delegate = self;
        service.tag = 2;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
      
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
        {
            NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
            
            NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
            
            [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
            [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
            [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
            [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"wishlist_id"];
            
            [self removeSingleVideo:METHOD_DELETE_LIST params:paramsDict];
            
//              NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"wishlist_id\":\"%@\",\"status\":\"%@\"}",strID,strToken,strClearID,@""];
//
//                [service executeWebserviceWithMethod:METHOD_DELETE_LIST withValues:strSend];
        }
        
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
            
            NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
            
            NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
            
            [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
            [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
            [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
            [paramsDict setObject:[dictLocal objectForKey:@"admin_video_id"] forKey:@"admin_video_id"];
            
            [self removeSingleVideo:METHOD_REMOVE_SPAM params:paramsDict];
//
//            NSData *postData = [NSJSONSerialization dataWithJSONObject:paramsDict options:0 error:&error];
//
////        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//                [[WebServiceHandler sharedInstance] sendRequest:METHOD_REMOVE_SPAM isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
//
//                    if ([[dict objectForKey:@"success"] intValue] == 1) {
//
//                        [self popup:[dict objectForKey:@"message"]];
//
//                    }
//
////                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                        for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
//                        {
//                            NSDictionary *dictLocal=[arrList objectAtIndex:i];                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"admin_video_id"]]]) {
//
//                                [arrList removeObjectAtIndex:i];
//
//                            }
//                        }
//
//                        [_collectionView reloadData];
//                        [self labelAndClearUpdate];
//
////                    });
//
//                }];
//
//
////            });
//
//            [[ProgressIndicator sharedInstance]hideProgressIndicator];

        }
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
        {
            NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
            
            NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
            
            [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
            [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
            [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
            [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"history_id"];
            
            [self removeSingleVideo:METHOD_DELETE_HISTORY params:paramsDict];
            
//              NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"history_id\":\"%@\",\"status\":\"%@\"}",strID,strToken,strClearID,@""];
//
//                [service executeWebserviceWithMethod:METHOD_DELETE_HISTORY withValues:strSend];
        }
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
}

- (void)removeSingleVideo:(NSString *)service params:(NSMutableDictionary *)params {
    
    __block NSError *error;

    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [[WebServiceHandler sharedInstance] sendRequest:METHOD_REMOVE_SPAM isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if ([[dict objectForKey:@"success"] intValue] == 1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self popup:[dict objectForKey:@"message"]];
                
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"admin_video_id"]]]) {
                        
                        [arrList removeObjectAtIndex:i];
                        
                        [_collectionView reloadData];
                        [self labelAndClearUpdate];
                        
                    }
                }
            });
            
            
        }
        
    }];
    
    
    //            });
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
     [refreshControl endRefreshing];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
     [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            nTotalCount=[[dictResponse valueForKey:@"total"] intValue];
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"wishlist"]];
            else
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"history"]];
            
            [self labelAndClearUpdate];
            
//            if (arrList.count!=0)
//            {
//                UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
//
//                if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
//                    lblTitle.text=@"My List";
//                else
//                    lblTitle.text=@"History";
//
//                [lblTitle setFont:[UIFont boldSystemFontOfSize:15]];
//                lblTitle.textColor=[UIColor whiteColor];
//                [self.view addSubview:lblTitle];
//
//                UIButton *btnClearAll=[UIButton buttonWithType:UIButtonTypeCustom];
//                btnClearAll.frame=CGRectMake(self.view.frame.size.width-110, 20,100, 30);
//                [btnClearAll setTitle:@"Clear all" forState:UIControlStateNormal];
//                [btnClearAll.titleLabel setFont:[UIFont systemFontOfSize:15]];
//                [btnClearAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                btnClearAll.backgroundColor=[UIColor colorWithRed:94.0f/255.0f green:94.0f/255.0f blue:100.0f/255.0f alpha:1];
//                [btnClearAll addTarget:self action:@selector(onClearAll:) forControlEvents:UIControlEventTouchDown];
//                [self.view addSubview:btnClearAll];
//
//                [_collectionView reloadData];
//            }
//            else
//            {
//                UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
//                imgeView.image=[UIImage imageNamed:@"oops.png"];
//                [self.view addSubview:imgeView];
//
//
//                UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
//                lblName.text=@"Oops!  List is empty ";
//                lblName.textAlignment=NSTextAlignmentCenter;
//                lblName.textColor=[UIColor whiteColor];
//                [lblName setFont:[UIFont systemFontOfSize:15]];
//                [self.view addSubview:lblName];
//            }
            
         //   [self onPageLoad];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle  message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            //strClearID
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            {
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"wishlist_id"]]]) {
                        
                         [arrList removeObjectAtIndex:i];
                        
                    }
                }
               
            }
            else
            {
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];
                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"history_id"]]]) {
                        
                        [arrList removeObjectAtIndex:i];
                    }
                }
            }
            [_collectionView reloadData];
           // [self onCallListWebService];
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                [alert addAction:defaultAction];
        
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"wishlist"]];
            else
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"history"]];
            [_collectionView reloadData];

        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle  message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
//    else if (webservice.tag==3)
//    {
//        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
//        {
//            dictSendVal=dictResponse;
//            [self performSegueWithIdentifier:@"singleVideo" sender:self];
//            
//        }
//        else
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Streaming" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }
//    }
    
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     if ([[segue identifier] isEqualToString:@"singleVideo"])
     {
         SingleVideoView *singleVideo=[segue destinationViewController];
         singleVideo.strVideoID=strAD_VideoID;
     }
 }


@end
