//
//  PaidVideos.m
//  Streaming
//
//  Created by Aravinth Ramesh on 04/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "PaidVideosVC.h"
#import "PaidVideosCell.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"

@interface PaidVideosVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *cardsArray;
}
@property (strong, nonatomic) IBOutlet UITableView *paidVideosTable;

@end

@implementation PaidVideosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    cardsArray = [NSMutableArray new];
    
    self.paidVideosTable.delegate = self;
    self.paidVideosTable.dataSource = self;
    
    self.paidVideosTable.rowHeight = UITableViewAutomaticDimension;
    self.paidVideosTable.estimatedRowHeight = 250;
    
    self.title = @"View Plans";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor ],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    
         [self callService:METHOD_PAID_VIDEOS params:postDataDict key:@"PaidVideos"];
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([dict objectForKey:@"success"]) {
                
                cardsArray = [dict objectForKey:@"data"];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.paidVideosTable reloadData];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                
            });
        }];
    });
}
#pragma mark TableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cardsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *paidVideoDetail = [cardsArray objectAtIndex:indexPath.row];
    
    PaidVideosCell *paidVideoCell = (PaidVideosCell *)[tableView dequeueReusableCellWithIdentifier:@"PaidVideosCell"];
    
    if (!paidVideoCell) {
        
        paidVideoCell = [[PaidVideosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaidVideosCell"];
    }
    
    NSURL *imageURL = [NSURL URLWithString:[paidVideoDetail objectForKey:@"picture"]];
    
    paidVideoCell.titleLabel.text = [paidVideoDetail objectForKey:@"title"];
    [paidVideoCell.videoImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    paidVideoCell.dateLabel.text = [paidVideoDetail objectForKey:@"paid_date"];
    paidVideoCell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[paidVideoDetail objectForKey:@"currency"],[paidVideoDetail objectForKey:@"amount"]];
    paidVideoCell.userTypeLabel.text = [paidVideoDetail objectForKey:@"type_of_user"];
    paidVideoCell.subscriptionTypeLabel.text = [paidVideoDetail objectForKey:@"type_of_subscription"];
    
    return paidVideoCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *paidVideoDetail = [cardsArray objectAtIndex:indexPath.row];
    
    SingleVideoView *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoView"];
    singleVideo.strVideoID = [paidVideoDetail objectForKey:@"admin_video_id"];
    [self.navigationController pushViewController:singleVideo animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
