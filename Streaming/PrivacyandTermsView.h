//
//  PrivacyandTermsView.h
//  Streaming
//
//  Created by KrishnaDev on 27/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyandTermsView : UIViewController

@property(strong,nonatomic)NSDictionary *dictValue;
- (IBAction)onBack:(id)sender;

@end
