//
//  PrivacyandTermsView.m
//  Streaming
//
//  Created by KrishnaDev on 27/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "PrivacyandTermsView.h"

@interface PrivacyandTermsView ()

@end

@implementation PrivacyandTermsView
@synthesize dictValue;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title=[dictValue valueForKey:@"heading"];
    
    UIScrollView *scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, self.view.frame.size.height-90)];
    scrollView.showsVerticalScrollIndicator=NO;
    scrollView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:scrollView];
    
//    {
//        UITextView *txtView=[[UITextView alloc]initWithFrame:CGRectMake(10, 70, self.view.frame.size.width-20, self.view.frame.size.height-90)];
//        txtView.layer.borderWidth = 1.0f;
//        txtView.layer.cornerRadius =2.0f;
//        txtView.clipsToBounds = YES;
//        txtView.backgroundColor=[UIColor clearColor];
//        txtView.userInteractionEnabled=NO;
//        [self.view addSubview:txtView];
//    }
    
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dictValue valueForKey:@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    UITextView *txtView=[[UITextView alloc]initWithFrame:CGRectMake(10,20, self.view.frame.size.width-20, 1000)];
    txtView.backgroundColor=[UIColor clearColor];
    // [txtView sizeToFit];
    txtView.userInteractionEnabled=NO;
    txtView.attributedText=attrStr;
    txtView.textColor=[UIColor whiteColor];
   txtView.textAlignment=NSTextAlignmentLeft;
   [txtView setFont:[UIFont fontWithName:@"Helvetica" size:15]];
//        txtView.layer.borderWidth = 1.0f;
//        txtView.layer.cornerRadius =2.0f;
//        txtView.clipsToBounds = YES;
   
    [txtView layoutIfNeeded];
   // txtView.layer.borderColor =  [[UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1] CGColor];
    // [txtView sizeToFit];
     [txtView sizeToFit];
    [scrollView addSubview:txtView];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width-20, txtView.frame.size.height+50);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
