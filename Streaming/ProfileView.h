//
//  ProfileView.h
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileView : UIViewController
{
   IBOutlet UIScrollView *scrollView;
    UIButton *btnChangePwd;
    UITextField *txtFirstName;
    UIButton *btnImage;
     UITextField *txtEmailID;
    UITextField *txtPhnNo;
    UIButton *btnEditAndSave;
}

//@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
//@property (weak, nonatomic) IBOutlet UIButton *btnImage;
//@property (weak, nonatomic) IBOutlet UIButton *btnEditAndSave;

//@property (weak, nonatomic) IBOutlet UITextField *txtEmailID;
//@property (weak, nonatomic) IBOutlet UITextField *txtPhnNo;

- (IBAction)onEditAndSave:(id)sender;
- (IBAction)onChangePassword:(id)sender;
- (IBAction)onChooseImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightSideButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *SideBarButton;
@end
