//
//  ProfileView.m
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "ProfileView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import "Webservice.h"
#import "ViewPlansVC.h"

@interface ProfileView ()<WebServiceDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
 
    NSMutableArray *arrImage;
    AppDelegate *appDelegate;
    int nValue;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    
    UITextField *currentTextView;

}

@end

@implementation ProfileView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    self.title=@"Profile";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    tapGesture.delegate = self;

    arrImage=[[NSMutableArray alloc]init];
    
    nValue=0;
   
    
    
    
//    [self.txtEmailID setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtEmailID.textColor=[UIColor whiteColor];
//    
//    [self.txtPhnNo setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtPhnNo.textColor=[UIColor whiteColor];
//    
//    [self.txtFirstName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtFirstName.textColor=[UIColor whiteColor];
    
   

    
    
    int xPos=0;
    int yPos=100;
    

    btnImage=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, yPos, 100, 100)];
    [btnImage setBackgroundImage:[UIImage imageNamed:@"Default.png"] forState:UIControlStateNormal];
    [btnImage addTarget:self action:@selector(onChooseImage:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnImage];
    btnImage.layer.cornerRadius = btnImage.frame.size.height / 2;
    btnImage.clipsToBounds = YES;
    
    [btnImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [btnImage.layer setBorderWidth: 2.0];
    
    yPos+=120;
    
    
    txtFirstName=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtFirstName setBorderStyle:UITextBorderStyleNone];
    txtFirstName.placeholder=@"Name";
    txtFirstName.delegate=self;
    [txtFirstName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtFirstName.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtFirstName];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    txtEmailID=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtEmailID setBorderStyle:UITextBorderStyleNone];
    txtEmailID.placeholder=@"Email";
    txtEmailID.delegate=self;
    txtEmailID.keyboardType=UIKeyboardTypeEmailAddress;
    [txtEmailID setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtEmailID.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtEmailID];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    txtPhnNo=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtPhnNo setBorderStyle:UITextBorderStyleNone];
    txtPhnNo.placeholder=@"Phone";
    txtPhnNo.delegate=self;
    txtPhnNo.keyboardType=UIKeyboardTypeNumberPad;
    [txtPhnNo setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtPhnNo.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtPhnNo];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    

    yPos+=30;
    
    btnEditAndSave=[UIButton buttonWithType:UIButtonTypeCustom];
    btnEditAndSave.frame=CGRectMake(20, yPos, self.view.frame.size.width-50, 40);
    [btnEditAndSave setTitle:@"Edit" forState:UIControlStateNormal];
    [btnEditAndSave.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnEditAndSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    btnEditAndSave.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:50.0f/255.0f blue:64.0f/255.0f alpha:1];
    [btnEditAndSave setBackgroundColor:[UIColor blackColor]];
    [btnEditAndSave addTarget:self action:@selector(onEditAndSave:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnEditAndSave];
    
    yPos+=45;
    
    btnChangePwd=[UIButton buttonWithType:UIButtonTypeCustom];
    btnChangePwd.frame=CGRectMake(20, yPos, self.view.frame.size.width-40, 50);
    [btnChangePwd setTitle:@"Change Password" forState:UIControlStateNormal];
    [btnChangePwd.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnChangePwd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    btnChangePwd.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:50.0f/255.0f blue:64.0f/255.0f alpha:1];
    [btnChangePwd setBackgroundColor:[UIColor blackColor]];
    [btnChangePwd addTarget:self action:@selector(onChangePassword:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnChangePwd];
    
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+50);
    
    
    
    txtFirstName.userInteractionEnabled=NO;
    txtEmailID.userInteractionEnabled=NO;
    txtPhnNo.userInteractionEnabled=NO;
    btnImage.userInteractionEnabled=NO;
    

    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
    NSUserDefaults *user1=[[NSUserDefaults alloc]init];
    
    NSString *strLoginType=[user1 valueForKey:@"logintype"];
    
    if ([strLoginType isEqualToString:@"2"])
    {
        btnChangePwd.hidden=YES;
        
    }
    
    btnImage.layer.cornerRadius = btnImage.frame.size.height / 2;
    btnImage.clipsToBounds = YES;
    
    [btnImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [btnImage.layer setBorderWidth: 2.0];
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    NSString *strID=[user valueForKey:@"id"];
    NSString *strToken=[user valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    NSString *strSend=[NSString stringWithFormat:@"?token=%@&id=%@",strToken,strID];
    
    [service executeWebserviceWithMethod1:METHOD_GET_USER_PROFILE withValues:strSend];
    

//    
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in scrollView.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    scrollView.contentSize = contentRect.size;
    
}

- (IBAction)viewPlansTapped:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"View Subscribed Plans ?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        ViewPlansVC *subsPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
        subsPlans.myPlans = YES;
        [self.navigationController pushViewController:subsPlans animated:YES];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    

}


-(void)dismissKeyBoard
{
    [txtEmailID resignFirstResponder];
    [txtFirstName resignFirstResponder];
    [txtPhnNo resignFirstResponder];
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onEditAndSave:(id)sender
{
  
    if (nValue==0)
    {
        nValue++;
        
        txtFirstName.userInteractionEnabled=YES;
        txtEmailID.userInteractionEnabled=YES;
        txtPhnNo.userInteractionEnabled=YES;
        btnImage.userInteractionEnabled=YES;
        [btnEditAndSave setTitle:@"Save" forState:UIControlStateNormal];
        
    }
    else
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        // [self presentViewController:alertController animated:YES completion:nil];
        
        NSString *emailRegEx =@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
        
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if ([txtFirstName.text isEqualToString:@""])
        {
            alertController.message=@"Please Enter  Name";
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
        else if ([txtEmailID.text isEqualToString:@""])
        {
            alertController.message=@"Please Enter Email id";
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
        else if([emailTest evaluateWithObject:txtEmailID.text] == NO)
        {
            alertController.message=@"Please enter valid email address.";
            [self presentViewController:alertController animated:YES completion:nil];
            //_txtEmail.text=@"";
            return;
        }
        else if ([txtPhnNo.text isEqualToString:@""])
        {
            alertController.message=@"Please Enter Phone Number";
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
        
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        
        Webservice *service=[[Webservice alloc]init];
        service.tag=2;
        service.delegate=self;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:txtEmailID.text forKey:@"email"];
        [_params setObject:strID forKey:@"id"];
        [_params setObject:txtFirstName.text forKey:@"name"];
        [_params setObject:txtPhnNo.text forKey:@"mobile"];
        [_params setObject:strToken forKey:@"token"];
        [_params setObject:appDelegate.strDeviceToken forKey:@"device_token"];
        
        
        
        //[_params setObject:strID forKey:@"height"];
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"picture";
        
        // the server url to which the image (or the media) is uploaded. Use your server url here
        
        
        // create request
        NSString *strURL = [SERVICE_URL stringByAppendingString:METHOD_PROFILE_UPDATE];
        NSURL *requestURL = [NSURL URLWithString:strURL];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        for (int i=0; i<arrImage.count; i++)
        {
            NSDictionary *dictLocal=[arrImage objectAtIndex:i];
            UIImage *image1 = [dictLocal objectForKey:@"UIImagePickerControllerOriginalImage"];
            //      NSData * imageData1 = UIImageJPEGRepresentation(image1,100);
            // add image data
            NSData *imageData = UIImageJPEGRepresentation(image1, 1.0);
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        
        [service executeWebserviceWithMethodinImage:METHOD_PROFILE_UPDATE withValues:request];
    }
    
   
}

- (IBAction)onChangePassword:(id)sender
{
    [self performSegueWithIdentifier:@"ChangePassword" sender:self];
}

- (IBAction)onChooseImage:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //Use camera if device has one otherwise use photo library
    //    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    //    {
    //        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    //        // imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    //        // picker.showsCameraControls = NO;
    //    }
    //    else
    //    {
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    // }
    
    [imagePicker setDelegate:self];
    
    //Show image picker
    [self presentModalViewController:imagePicker animated:YES];

}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [arrImage removeAllObjects];
    [arrImage addObject:info];
    
    UIImage *image1 = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [btnImage setBackgroundImage:image1 forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
           
            NSURL *pictureURL = [NSURL URLWithString:[dictResponse valueForKey:@"picture"]];
            NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
            UIImage *fbImage = [UIImage imageWithData:imageData];
            [btnImage setBackgroundImage:fbImage forState:UIControlStateNormal];
            txtFirstName.text=[dictResponse valueForKey:@"name"];
            txtEmailID.text=[dictResponse valueForKey:@"email"];
            txtPhnNo.text=[dictResponse valueForKey:@"mobile"];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
        
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            nValue--;
            txtFirstName.userInteractionEnabled=NO;
            txtEmailID.userInteractionEnabled=NO;
            txtPhnNo.userInteractionEnabled=NO;
            btnImage.userInteractionEnabled=NO;
            [btnEditAndSave setTitle:@"Edit" forState:UIControlStateNormal];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle  message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
        
    }
    
    
}
#pragma mark -- Keyboard hide  and moving

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    //    if (!textField.inputAccessoryView)
    //    {
    //        textField.inputAccessoryView = toolKeyboard;
    //    }
    currentTextView=textField;
    
    //    [self moveViewUp:YES toRect:textField.frame];
    [self moveScrollView:textField.frame];
}

- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [scrollView setContentOffset:scrollPoint animated:YES];
    }
}


@end
