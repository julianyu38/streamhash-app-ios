//
//  RegisterView.h
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>


@interface RegisterView : UIViewController<WebServiceDelegate, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate>
{
    IBOutlet UIScrollView *scrollView;
    UITextField *txtName;
    UITextField *txtEmail;
    UITextField *txtPhone;
    UITextField *txtPassword;
    UIButton *btnImage;
}

//@property (weak, nonatomic) IBOutlet UITextField *txtName;
//@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
//@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
//@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

- (IBAction)onSignup:(id)sender;
//@property (weak, nonatomic) IBOutlet UIButton *btnImage;
- (IBAction)onChooseImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
- (IBAction)onFacebook:(id)sender;

- (IBAction)onGoogle:(id)sender;
-(IBAction)onBack:(id)sender;
@end
