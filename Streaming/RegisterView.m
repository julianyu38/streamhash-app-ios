//
//  RegisterView.m
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "RegisterView.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#define thumbSize CGSizeMake(130, 150)

@interface RegisterView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    NSMutableArray *arrImage;
    AppDelegate *appDelegate;
    
    NSString *strProfileImg,*strID,*strName,*strEmailID,*strLoginType;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    
    UITextField *currentTextView;

}

@end

@implementation RegisterView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title=@"Signup";
    
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    
    arrImage=[[NSMutableArray alloc]init];
   

    
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    // [signIn setClientID: kClientId];
    [signIn setScopes:[NSArray arrayWithObject:@"https://www.googleapis.com/auth/plus.login"]];
    [signIn setDelegate: self];
    //NSLog(@"Initialized auth2...");
    
    
//    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
//    tapRecognizer1.cancelsTouchesInView = NO;
//    [self.view addGestureRecognizer:tapRecognizer1];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    tapGesture.delegate = self;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    
//    [self.txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtEmail.textColor=[UIColor whiteColor];
//    
//    [self.txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtPassword.textColor=[UIColor whiteColor];
//   
//    [self.txtName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtName.textColor=[UIColor whiteColor];
//    
//    [self.txtPhone setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    _txtPhone.textColor=[UIColor whiteColor];
//    
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in scrollView.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    scrollView.contentSize = contentRect.size;
    
    
    
    int xPos=0;
    int yPos=100;
    
    UIButton *btnGoogle=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-155, yPos, 150, 40)];
    [btnGoogle setBackgroundImage:[UIImage imageNamed:@"g_plus.png"] forState:UIControlStateNormal];
    [btnGoogle addTarget:self action:@selector(onGoogle:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnGoogle];
    
    UIButton *btnFacebook=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+5, yPos, 150, 40)];
    [btnFacebook setBackgroundImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
    [btnFacebook addTarget:self action:@selector(onFacebook:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnFacebook];
    
    yPos+=80;
    
    
    
    btnImage=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, yPos, 100, 100)];
    [btnImage setBackgroundImage:[UIImage imageNamed:@"Default.png"] forState:UIControlStateNormal];
    [btnImage addTarget:self action:@selector(onChooseImage:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnImage];
    btnImage.layer.cornerRadius = btnImage.frame.size.height / 2;
    btnImage.clipsToBounds = YES;
    
    [btnImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [btnImage.layer setBorderWidth: 2.0];
    
    yPos+=120;
    
    
    txtName=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtName setBorderStyle:UITextBorderStyleNone];
    txtName.placeholder=@"Name";
    txtName.delegate=self;
    [txtName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtName.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtName];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtEmail setBorderStyle:UITextBorderStyleNone];
    txtEmail.placeholder=@"Email";
    txtEmail.delegate=self;
    txtEmail.keyboardType=UIKeyboardTypeEmailAddress;
    [txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtEmail.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtEmail];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    txtPhone=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtPhone setBorderStyle:UITextBorderStyleNone];
    txtPhone.placeholder=@"Phone";
    txtPhone.delegate=self;
    txtPhone.keyboardType=UIKeyboardTypeNumberPad;
    [txtPhone setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtPhone.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtPhone];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    txtPassword=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtPassword setBorderStyle:UITextBorderStyleNone];
    txtPassword.placeholder=@"Password";
    txtPassword.delegate=self;
    txtPassword.secureTextEntry=YES;
    [txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtPassword.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtPassword];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=5;
    }
    
    yPos+=30;
    
    UIButton *btnSignUp=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSignUp.frame=CGRectMake(20, yPos, self.view.frame.size.width-40, 50);
    [btnSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [btnSignUp.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    btnSignUp.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:50.0f/255.0f blue:64.0f/255.0f alpha:1];
    [btnSignUp setBackgroundColor:[UIColor blackColor]];
    [btnSignUp addTarget:self action:@selector(onSignup:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnSignUp];

    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+50);

}
-(IBAction)onBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)dismissKeyBoard
{
    [txtName resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtPhone resignFirstResponder];
}
- (IBAction)onSignup:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    // [self presentViewController:alertController animated:YES completion:nil];
    
    NSString *emailRegEx =@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([txtName.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Name";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([txtEmail.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Email id";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if([emailTest evaluateWithObject:txtEmail.text] == NO)
    {
        alertController.message=@"Please enter valid email address.";
        [self presentViewController:alertController animated:YES completion:nil];
        //_txtEmail.text=@"";
        return;
    }
    
    else if ([txtPassword.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([txtPhone.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Phone Number";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    
    
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    
    Webservice *service=[[Webservice alloc]init];
    service.tag=1;
    service.delegate=self;
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:txtEmail.text forKey:@"email"];
    [_params setObject:txtPassword.text forKey:@"password"];
    [_params setObject:txtName.text forKey:@"name"];
    [_params setObject:txtPhone.text forKey:@"mobile"];
    [_params setObject:@"manual" forKey:@"login_by"];
    [_params setObject:@"ios" forKey:@"device_type"];
    [_params setObject:appDelegate.strDeviceToken forKey:@"device_token"];
    
  
    //[_params setObject:strID forKey:@"height"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"picture";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    
    
    // create request
    NSString *strURL = [SERVICE_URL stringByAppendingString:METHOD_REGISTER];
    NSURL *requestURL = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
   
    for (int i=0; i<arrImage.count; i++)
    {
        NSDictionary *dictLocal=[arrImage objectAtIndex:i];
        UIImage *image1 = [dictLocal objectForKey:@"UIImagePickerControllerOriginalImage"];
        //      NSData * imageData1 = UIImageJPEGRepresentation(image1,100);
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(image1, 1.0);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [service executeWebserviceWithMethodinImage:METHOD_REGISTER withValues:request];
    
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    [user setObject:@"1"  forKey:@"logintype"];
    [user synchronize];
      
}
- (IBAction)onChooseImage:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //Use camera if device has one otherwise use photo library
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//    {
//        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
//        // imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
//        // picker.showsCameraControls = NO;
//    }
//    else
//    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
   // }
    
    [imagePicker setDelegate:self];
    
    //Show image picker
    [self presentModalViewController:imagePicker animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [arrImage removeAllObjects];
    [arrImage addObject:info];
    
    UIImage *image1 = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [btnImage setBackgroundImage:image1 forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            NSUserDefaults *user=[[NSUserDefaults alloc]init];
            [user setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"id"]]  forKey:@"id"];
            [user setObject:[dictResponse valueForKey:@"name"]  forKey:@"name"];
            [user setObject:[dictResponse valueForKey:@"picture"]  forKey:@"picture"];
            [user setObject:[dictResponse valueForKey:@"token"]  forKey:@"token"];
             [user setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"push_status"]]  forKey:@"push"];
            //[user setObject:[dictResponse valueForKey:@"status"]  forKey:@"status"];
            [user synchronize];
            
            //            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //            [defaults setValue:[dictResponse valueForKey:@"username"] forKey:@"username"];
            //            [defaults synchronize];
            //            //appDelegate.strusername=[dictResponse valueForKey:@"username"];
            //
            //            NSUserDefaults *defaults1 =[NSUserDefaults standardUserDefaults];
            //            [defaults1 setValue:[dictResponse valueForKey:@"UserIdentity"] forKey:@"UserIdentity"];
            //
                       [self performSegueWithIdentifier:@"login_menu" sender:self];
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
    
    
}


- (IBAction)onGoogle:(id)sender {
    
    [[GIDSignIn sharedInstance] signOut];
    
    [[GIDSignIn sharedInstance] signIn];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    if (error)
    {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
    }
    else
        
    {
        
        //NSLog(@"User Id: %@",user.userID);
        //NSLog(@"Name: %@",user.profile.name);
        //NSLog(@"Email: %@",user.profile.email);
        
        strName=user.profile.name;
        strEmailID=user.profile.email;
        strID=user.userID;
        strLoginType=@"google";
        
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            strProfileImg=imageURL.absoluteString;
            //NSLog(@"Image URL :%@",imageURL);
        }
        
        
        [self onSocialLogin];
        
    }
    
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    if (error) {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
        return;
    }
}



-(IBAction)onFacebook:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
        if (error) {
            //NSLog(@"Process error");
        }
        else if (result.isCancelled)
        {
            //NSLog(@"Cancelled");
        }
        else
        {
            //NSLog(@"Logged in");
            
           // [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                          initWithGraphPath:@"/me"
                                          parameters:@{ @"fields": @"id,email,name,gender,birthday"}
                                          HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             
             
             {
                 if (!error) {
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setValue:result[@"email"] forKey:@"Email"];
                     
                     
                     
                     strName=result[@"name"];
                     strEmailID=result[@"email"];
                     strID=result[@"id"];
                     strProfileImg=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", strID];
                     strLoginType=@"facebook";
                     
                     
                     //NSLog(@"fetched user:%@", result);
                     [self onSocialLogin];
                     
                     
                 }
                 
             }];
            
        }
    }];
    
    
    
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}
-(void)onSocialLogin
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strSend=[NSString stringWithFormat:@"{\"social_unique_id\":\"%@\",\"email\":\"%@\",\"picture\":\"%@\",\"mobile\":\"%@\",\"login_by\":\"%@\",\"device_token\":\"%@\",\"device_type\":\"%@\",\"name\":\"%@\"}",strID,strEmailID,strProfileImg,@"",strLoginType,appDelegate.strDeviceToken,@"ios",strName];
    
    [service executeWebserviceWithMethod:METHOD_REGISTER withValues:strSend];
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    [user setObject:@"2"  forKey:@"logintype"];
    [user synchronize];
    
    
}
#pragma mark -- Keyboard hide  and moving

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    //    if (!textField.inputAccessoryView)
    //    {
    //        textField.inputAccessoryView = toolKeyboard;
    //    }
    currentTextView=textField;
    
    //    [self moveViewUp:YES toRect:textField.frame];
    [self moveScrollView:textField.frame];
}

- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

@end
