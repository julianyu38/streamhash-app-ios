//
//  SearchBarVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 04/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "SearchBarVC.h"
#import "WebServiceHandler.h"
#import "UIImageView+WebCache.h"
#import "ProgressIndicator.h"
#import "SearchVideosCell.h"

@interface SearchBarVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>
{
    NSString *searchKey;
    NSMutableArray *videosArray;
    NSArray *dataArray;
    NSUserDefaults *defaults;
    NSMutableDictionary *postDataDict;
    
}

@property (strong, nonatomic) IBOutlet UISearchBar *videoSearchBar;

@property (strong, nonatomic) IBOutlet UICollectionView *searchCollectionView;

@end

@implementation SearchBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchKey = @"";
    videosArray = [NSMutableArray new];
    
    self.title = @"Search Videos";
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor ],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.searchCollectionView.delegate = self;
    self.searchCollectionView.dataSource = self;
    
    self.videoSearchBar.delegate = self;
    
    defaults = [[NSUserDefaults alloc] init];
    
    postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    
    [self callService:METHOD_SEARCH_VIDEO params:postDataDict];
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;

}

#pragma mark Searching

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self contains[c] %@", searchText];
//    dataArray = [videosArray filteredArrayUsingPredicate:resultPredicate];
//}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
//    if (searchText.length > 0) {
    
        searchKey = searchText;
        [postDataDict setObject:searchKey forKey:@"key"];

        [self callService:METHOD_SEARCH_VIDEO params:postDataDict];
//    }
}

#pragma mark CollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return videosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *videoDetails = [videosArray objectAtIndex:indexPath.row];
    SearchVideosCell *cell = (SearchVideosCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchVideosCell" forIndexPath:indexPath];
    
    cell.videoImageView.layer.cornerRadius = 5;
    cell.cellBackView.layer.cornerRadius = 5;
    
    
    NSURL *imgURL = [NSURL URLWithString:[videoDetails objectForKey:@"default_image"]];
    [cell.videoImageView sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    cell.videoTitleLabel.text = [videoDetails objectForKey:@"title"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.searchCollectionView.frame.size.width/3-8, 190);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


// Service call
- (void)callService:(NSString *)service params:(NSMutableDictionary *)params{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                
                videosArray = [dict objectForKey:@"data"];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (videosArray) {
                    [self.searchCollectionView reloadData];
                }else{
                    [self popup:@"No videos found"];
                }
                
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                
            });
        }];
    });
}

- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
