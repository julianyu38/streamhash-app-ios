//
//  SearchView.h
//  Streaming
//
//  Created by Ramesh on 02/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIViewController
{
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *tableViewSearch;
    IBOutlet UISearchController *searchController;
}

@end
