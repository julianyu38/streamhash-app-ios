//
//  SearchView.m
//  Streaming
//
//  Created by Ramesh on 02/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "SearchView.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import "SingleVideoView.h"

@interface SearchView ()<UITableViewDataSource,UITableViewDelegate,UISearchControllerDelegate,WebServiceDelegate>
{
    NSMutableArray *arrList;
    
    NSArray *contentList;
    NSMutableArray *filteredContentList;
    BOOL isSearching;
    
    AppDelegate *appDelegate;
    NSDictionary *dictSendVal;
    
    NSString *strAD_VideoID;
}

@end

@implementation SearchView

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    self.title=@"Search";
    
    arrList=[[NSMutableArray alloc]init];
    filteredContentList = [[NSMutableArray alloc] init];
 //   [searchBar becomeFirstResponder];
    
   // contentList = [[NSMutableArray alloc] initWithObjects:@"iPhone", @"iPod", @"iPod touch", @"iMac", @"Mac Pro", @"iBook",@"MacBook", @"MacBook Pro", @"PowerBook", nil];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    NSString *subProfileID = [defaluts valueForKey:@"sub_profile_id"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"sub_profile_id\":\"%@\"}",strID,strToken,subProfileID];
    
    [service executeWebserviceWithMethod:METHOD_SEARCH_VIDEO withValues:strSend];

    
    // Do any additional setup after loading the view.
    
//    [self.searchDisplayController setActive: YES animated: YES];
//    self.searchDisplayController.searchBar.hidden = NO;
//    [self.searchDisplayController.searchBar becomeFirstResponder];
//    
//     [self.searchDisplayController.searchResultsTableView reloadData];
}
-(void)viewWillAppear:(BOOL)animated
{
//    [[UIDevice currentDevice] setValue:
//     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
//                                forKey:@"orientation"];
//     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
}

-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.navigationController popViewControllerAnimated:YES];
    // DO ur operations
}
#pragma mark -- tableview Delegate and Data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
     return [filteredContentList count];
   }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
  
        cell.textLabel.text = [[filteredContentList objectAtIndex:indexPath.row] valueForKey:@"title"];
    cell.textLabel.textColor=[UIColor whiteColor];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellStyleDefault;
   
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSDictionary *dictLocal=[filteredContentList objectAtIndex:indexPath.row];
    
    
    NSString *strVideoID=[dictLocal valueForKey:@"admin_video_id"];
    strAD_VideoID=strVideoID;
    [self performSegueWithIdentifier:@"singleVideo" sender:self];
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.delegate=self;
//    service.tag=2;
//    
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    NSString *strID=[defaluts valueForKey:@"id"];
//    NSString *strToken=[defaluts valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
//    
//    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];
    
    
}

- (void)searchTableList {
    NSString *searchString = searchBar.text;
    
    if([searchString length] != 0) {
        for(NSDictionary * dictValue in contentList)
        {
            NSString * strVideoName = [dictValue objectForKey:@"title"];
            NSRange VideoNameRange = [[strVideoName lowercaseString] rangeOfString:[searchString lowercaseString]];
            
            if(VideoNameRange.location != NSNotFound)
                [filteredContentList addObject:dictValue];
        }
        //[self searchTableList];
    }
    else {
        filteredContentList=[NSMutableArray arrayWithArray:contentList];
    }
    
    [tableViewSearch reloadData];

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //NSLog(@"Text change - %d",isSearching);
    
    //Remove all objects first.
    [filteredContentList removeAllObjects];
    
    if([searchText length] != 0) {
           for(NSDictionary * dictValue in contentList)
            {
                NSString * strVideoName = [dictValue objectForKey:@"title"];
                NSRange VideoNameRange = [[strVideoName lowercaseString] rangeOfString:[searchText lowercaseString]];
                
                if(VideoNameRange.location != NSNotFound)
                    [filteredContentList addObject:dictValue];
            }
            //[self searchTableList];
    }
    else {
        filteredContentList=[NSMutableArray arrayWithArray:contentList];
    }
    
    [tableViewSearch reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    //NSLog(@"Search Clicked");
    [self searchTableList];
}

#pragma mark --  Search Bar



#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            contentList=[dictResponse valueForKey:@"data"];
            filteredContentList=[NSMutableArray arrayWithArray:contentList];
        
          //  [self.searchDisplayController.searchResultsTableView reloadData];
            [tableViewSearch reloadData];
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Video Steaming" message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
        
    }
//    else if (webservice.tag==2)
//    {
//        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
//        {
//            dictSendVal=dictResponse;
//            [self performSegueWithIdentifier:@"singleVideo" sender:self];
//            
//            
//        }
//        else
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }
//    }
    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
        singleVideo.strVideoID=strAD_VideoID;
        
    }
}


@end
