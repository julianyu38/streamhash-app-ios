//
//  SettingsView.m
//  Streaming
//
//  Created by Ramesh on 17/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "SettingsView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "PrivacyandTermsView.h"
#import "WebServiceHandler.h"
#import "ViewPlansVC.h"
#import "PaidVideosVC.h"


@interface SettingsView ()<WebServiceDelegate>
{
    AppDelegate *appDelegate;
    NSDictionary *dictSend;
}

@end

@implementation SettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    self.title=@"Settings";
    
    viewPrivacy.layer.shadowOpacity = 0.6f;
    viewPrivacy.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    viewPrivacy.layer.shadowRadius = 2.0f;
    viewPrivacy.layer.cornerRadius = 3.0f;
    
    viewNotication.layer.shadowOpacity = 0.6f;
    viewNotication.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    viewNotication.layer.shadowRadius = 2.0f;
    viewNotication.layer.cornerRadius = 3.0f;
    
    viewDeleteAccount.layer.shadowOpacity = 0.6f;
    viewDeleteAccount.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    viewDeleteAccount.layer.shadowRadius = 2.0f;
    viewDeleteAccount.layer.cornerRadius = 3.0f;
    
    viewPlansView.layer.shadowOpacity = 0.6f;
    viewPlansView.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    viewPlansView.layer.shadowRadius = 2.0f;
    viewPlansView.layer.cornerRadius = 3.0f;
    
    paidVideosView.layer.shadowOpacity = 0.6f;
    paidVideosView.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    paidVideosView.layer.shadowRadius = 2.0f;
    paidVideosView.layer.cornerRadius = 3.0f;
    
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    NSString *strID=[user valueForKey:@"id"];
    NSString *strToken=[user valueForKey:@"token"];
    
      NSString *strPush=[user valueForKey:@"push"];
    
    if ([strPush isEqualToString:@"1"])
    {
        [switchBtn setOn:YES animated:YES];
    }
    else
    {
        [switchBtn setOn:NO animated:YES];

    }
    
}

- (IBAction)viewPlansTapped:(id)sender {
    ViewPlansVC *viewPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
    
    [self.navigationController pushViewController:viewPlans animated:YES];
}
- (IBAction)paidVideosTapped:(id)sender {
    
    PaidVideosVC *paidVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"PaidVideosVC"];
    paidVideo.title = @"Paid Videos";
    [self.navigationController pushViewController:paidVideo animated:YES];

}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue] == true)
        {
            dictSend=[dictResponse valueForKey:@"page"];
            [self performSegueWithIdentifier:@"details" sender:self];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle  message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
        
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
           
             NSUserDefaults *user=[[NSUserDefaults alloc]init];
            [user setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"push_status"]] forKey:@"push"];
            
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
        
    }
    else if (webservice.tag==3)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [appDelegate onExpriedPage];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"details"])
    {
        PrivacyandTermsView *privacy=[segue destinationViewController];
        privacy.dictValue=dictSend;
    }
    
}


- (IBAction)onPrivacy:(id)sender
{
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    NSString *strID=[user valueForKey:@"id"];
    NSString *strToken=[user valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    NSString *strSend=[NSString stringWithFormat:@"?%@&=%@",strID,strToken];
    
    [service executeWebserviceWithMethod1:METHOD_PRIVACY withValues:strSend];
}

- (IBAction)onTerms:(id)sender
{
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    NSString *strID=[user valueForKey:@"id"];
    NSString *strToken=[user valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    NSString *strSend=[NSString stringWithFormat:@"?%@&=%@",strID,strToken];
    
    [service executeWebserviceWithMethod1:METHOD_TERMS withValues:strSend];
    
}

- (IBAction)switchToggled:(id)sender
{
    UISwitch *mySwitch = (UISwitch *)sender;
    NSString *strSend;
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    if ([mySwitch isOn])
    {
        strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"1"];
         //NSLog(@"its on!");
    } else
    {
        
     strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"0"];
        
       //NSLog(@"its off!");
    }//METHOD_NOTIFICATION
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=2;
    [service executeWebserviceWithMethod:METHOD_NOTIFICATION withValues:strSend];

}

- (IBAction)onDelegate:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:@"Do you want delete your account" preferredStyle:UIAlertControllerStyleAlert]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=3;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strID,strToken];
        
        [service executeWebserviceWithMethod:METHOD_DELETE withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        
        
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11

}
@end
