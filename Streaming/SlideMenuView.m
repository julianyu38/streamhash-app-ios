//
//  SlideMenuView.m
//  Royal Water
//
//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import "SlideMenuView.h"
#import "AppDelegate.h"
#import "ExpandableTableViewCell.h"
#import "ProgressIndicator.h"
#import "SWRevealViewController.h"
#import "ManageProfileVC.h"
#import "CardsVC.h"

@interface SlideMenuView ()
{
    NSMutableArray *arrUserMenuList;
    AppDelegate *appDelegate;
    NSArray *arrGetCategory;
    
}
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIImageView *sideMenuLogo;
@property (strong, nonatomic) IBOutlet UIButton *addProfileButton;
@property (strong, nonatomic) IBOutlet UIButton *profilePic;

@end

@implementation SlideMenuView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.backButton.frame = CGRectMake(10, 20, 25, 25);
    self.sideMenuLogo.frame = CGRectMake(self.backButton.frame.origin.x+30, self.backButton.frame.origin.y-5, 70, 35);
    
    // Hide side menu
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [self.backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    // Add Profile
//    self.addProfileButton.frame = CGRectMake(self.view.frame.origin.x+20, self
//                                             .sideMenuLogo.frame.size.height+30, 30, 30);
    self.addProfileButton.frame = CGRectMake(imgProfile.frame.origin.x+120, imgProfile.frame.origin.y, 30, 30);
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  

//    NSUserDefaults *user=[[NSUserDefaults alloc]init];
//    NSString *strName=[user valueForKey:@"name"];
//    NSString *strPic=[user valueForKey:@"picture"];
//
//    if ([strName length]!=0)
//        lblProfileName.text=strName;
//
//    if ([strPic length]!=0)
//    {
//         NSURL *pictureURL = [NSURL URLWithString:strPic];
//        NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
//        UIImage *fbImage = [UIImage imageWithData:imageData];
//        [imgProfile setImage:fbImage];
//    }
  
    arrUserMenuList=[[NSMutableArray alloc]init];
    
    //  UserPart
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2;
    imgProfile.clipsToBounds = YES;
   
    [imgProfile.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [imgProfile.layer setBorderWidth: 2.0];
    
    
    [self onCreateMenuItems];
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.tag=1;
//    service.delegate=self;
//    
//    NSUserDefaults *user1=[[NSUserDefaults alloc]init];
//    NSString *strId=[user1 valueForKey:@"id"];
//    NSString *strToken=[user1 valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strId,strToken];
//    [service executeWebserviceWithMethod:METHOD_GET_CATEGORIES withValues:strSend];
    

    
   // arrUserMenuList= @[@"Home",@"Order History",@"Pending Orders",@"Notifications",@"Cash Management",@"New Customer Referal",@"Stock History",@"Stock Point",@"Log Off"];
   
//    
//    NSArray *arrMenu=@[@{@"name":@"Home",@"ImageURL":@"",@"level":@"0"},@{@"name":@"My List",@"ImageURL":@"",@"level":@"0"},@{@"name":@"Categories",@"ImageURL":@"",@"SubMenu":@[@{@"name":@"Entertainment",@"level":@"1"},@{@"name":@"Sports",@"level":@"1"},@{@"name":@"Fashion",@"level":@"1"},@{@"name":@"Automative",@"level":@"1"}]},@{@"name":@"History",@"ImageURL":@"",@"level":@"0"},@{@"name":@"Settings",@"ImageURL":@"",@"level":@"0"},@{@"name":@"Share",@"ImageURL":@"",@"level":@"0"},@{@"name":@"Logout",@"ImageURL":@"",@"level":@"0"}];
//    
//    arrUserMenuList=[NSMutableArray arrayWithArray:arrMenu];
  
    
}

- (IBAction)backButtonTapped:(id)sender {

}

- (IBAction)addProfileTapped:(id)sender {
    
    ManageProfileVC *manageProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageProfileVC"];
    [self.navigationController pushViewController:manageProfile animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
  //  [self onCreateMenuItems];
    self.navigationController.navigationBar.hidden = YES;
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    NSString *strName=[user valueForKey:@"name"];
    NSString *strPic=[user valueForKey:@"picture"];
    
    if ([strName length]!=0)
        lblProfileName.text=strName;
    
    if ([strPic length]!=0)
    {
        NSURL *pictureURL = [NSURL URLWithString:strPic];
        NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
        UIImage *fbImage = [UIImage imageWithData:imageData];
        [imgProfile setImage:fbImage];
    }
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    //     id , name, picture  ,token
//    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.tag=1;
//    service.delegate=self;
//    
//    NSUserDefaults *user=[[NSUserDefaults alloc]init];
//    NSString *strId=[user valueForKey:@"id"];
//    NSString *strToken=[user valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strId,strToken];
//    [service executeWebserviceWithMethod:METHOD_GET_CATEGORIES withValues:strSend];
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onEditProfile:(id)sender
{
    [self performSegueWithIdentifier:@"profileEdit" sender:self];
}
-(void)onCreateMenuItems
{
    NSMutableArray *arrLocal=[[NSMutableArray alloc]init];
    arrLocal=[NSMutableArray arrayWithArray:appDelegate.arrMenuCategoryList];
    for (int i=0; i<arrLocal.count; i++)
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[arrLocal objectAtIndex:i];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"1" forKey:@"level"];
        [arrLocal replaceObjectAtIndex:i withObject:newDict];
    }
    
//    NSMutableDictionary *dictLocal=[[NSMutableDictionary alloc]init];
//    [dictLocal setObject:@"1" forKey:@"level"];
//    [dictLocal setObject:@"ViewAll" forKey:@"name"];
//    
//    [arrLocal addObject:dictLocal];
    
  //  NSArray *arrMenu=@[@{@"name":@"Home",@"ImageURL":@"home.png",@"level":@"0"},@{@"name":@"My List",@"ImageURL":@"favorite1.png",@"level":@"0"},@{@"name":@"Categories",@"ImageURL":@"category.png",@"SubMenu":arrLocal},@{@"name":@"History",@"ImageURL":@"history.png",@"level":@"0"},@{@"name":@"Settings",@"ImageURL":@"settings1.png",@"level":@"0"},@{@"name":@"Share",@"ImageURL":@"share1.png",@"level":@"0"},@{@"name":@"Logout",@"ImageURL":@"logout1.png",@"level":@"0"}];
    
     NSArray *arrMenu=@[@{@"name":@"Home",@"ImageURL":@"home_new.png",@"level":@"0"},@{@"name":@"My List",@"ImageURL":@"heart_outline_new.png",@"level":@"0"},@{@"name":@"Categories",@"ImageURL":@"star_outline_new.png",@"SubMenu":arrLocal},@{@"name":@"History",@"ImageURL":@"history_new.png",@"level":@"0"},@{@"name":@"Spam",@"ImageURL":@"spam_new.png",@"level":@"0"},@{@"name":@"Cards",@"ImageURL":@"credit_card_new.png",@"level":@"0"},@{@"name":@"Settings",@"ImageURL":@"settings_new.png",@"level":@"0"},@{@"name":@"Logout",@"ImageURL":@"logout_new.png",@"level":@"0"}];
    
    arrUserMenuList=[NSMutableArray arrayWithArray:arrMenu];
    
    
    [self.menuTableView reloadData];
    
}

//#pragma mark
//#pragma mark - WebSerive Delegate methods
//
//-(void)receivedErrorWithMessage:(NSString *)message
//{
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
//    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//    [alertController addAction:ok];
//    
//    [self presentViewController:alertController animated:YES completion:nil];
//
//}
//-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
//{
//      [[ProgressIndicator sharedInstance]hideProgressIndicator];
//    if (webservice.tag==1)
//    {
//        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
//        {
//            arrGetCategory=[dictResponse valueForKey:@"categories"];
//            [self onCreateMenuItems];
//            
//        }
//        else
//        {
//            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
//            {
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Video Steaming" message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
//                
//                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                    
//                    [appDelegate onExpriedPage];
//                    
//                }]; // 8
//                
//                [alert addAction:defaultAction];
//                
//                
//                
//                [self presentViewController:alert animated:YES completion:nil]; // 11
//            }
//            else
//            {
//                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alertController addAction:ok];
//                [self presentViewController:alertController animated:YES completion:nil];
//                
//            }
//        }
//        
//    }
//    
//}


#pragma mark
#pragma mark - Table view data sources


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return arrUserMenuList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *Title = [[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    
     return [self createCellWithTitle:Title image:[[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"ImageURL"] indexPath:indexPath];
    
//    UITableViewCell *cell;
//    
//    //    if ([CellIdentifier isEqualToString:@"Logout"])
//    //    {
//    //        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    //        if ([appDelegate.strEmailID isEqualToString:@""])
//    //        {
//    //            UILabel *lblExit=[[UILabel alloc]initWithFrame:CGRectMake(62, 03, 150, 30)];
//    //            lblExit.text=@"Exit";
//    //            [lblExit setFont:[UIFont fontWithname:@"Avenir Next-Bold" size:19]];
//    //            lblExit.userInteractionEnabled=NO;
//    //            lblExit.backgroundColor=[UIColor whiteColor];
//    //            lblExit.textColor=[UIColor darkGrayColor];
//    //            [cell addSubview:lblExit];
//    //        }
//    //
//    //        // cell.textLabel.text=@"Exit";
//    //    }
//    //    else
//    //    {
//    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//    //}
//    //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSString *CellIdentifier = [arrUserMenuList objectAtIndex:indexPath.row];
    
    NSDictionary *dic=[arrUserMenuList objectAtIndex:indexPath.row];
    if([dic valueForKey:@"SubMenu"])
    {
        NSArray *arr=[dic valueForKey:@"SubMenu"];
        BOOL isTableExpanded=NO;
        
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[arrUserMenuList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [arrUserMenuList insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }

    if ([[dic valueForKey:@"name"] isEqualToString:@"Logout"])
    {
        
        NSUserDefaults *user=[[NSUserDefaults alloc]init];
        [user setObject:@""  forKey:@"id"];
        [user setObject:@""  forKey:@"token"];
        [user synchronize];
        
        [appDelegate onExpriedPage];
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Home"])
    {
        
        [self performSegueWithIdentifier:@"Home" sender:self];
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"My List"])
    {
        appDelegate.strListORHistroyORSpam=@"My List";
        [self performSegueWithIdentifier:@"myList" sender:self];
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"History"])
    {
        appDelegate.strListORHistroyORSpam=@"History";
        [self performSegueWithIdentifier:@"myList" sender:self];
       
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Settings"])
    {
        
        [self performSegueWithIdentifier:@"Settings" sender:self];
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Share"])
    {
        NSString *string = [NSString stringWithFormat:@"%@ Invited you to install Stream Hash", @"Ramesh"];
        
        // NSURL *URL = [NSURL URLWithString:@"www.apppoets.com/"];
        
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com"]];
        
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc]
                                                            initWithActivityItems:@[string,URL]
                                                            applicationActivities:nil];
        
        [activityViewController setCompletionWithItemsHandler:^(NSString *activityType,
                                                                
                                                                BOOL completed,
                                                                
                                                                NSArray *returnedItems,
                                                                
                                                                NSError * error){
            
            if ( completed )
            {
                //  TODO error handling
                //NSLog(@"share complete");
                //            NSExtensionItem* extensionItem = [returnedItems firstObject];
                //            NSItemProvider* itemProvider = [[extensionItem attachments] firstObject];
                
            } else {
                
                //NSLog(@"canceld");
                
            }
            
        }];
        
        if ( [activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
            
            // iOS8
            activityViewController.popoverPresentationController.sourceView = self.view;
            
            //        activityViewController.popoverPresentationController.sourceRect = self.view.bounds;
        }
        
        [self presentViewController:activityViewController animated:YES completion:nil];
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Categories"])
    {
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Spam"])
    {
        appDelegate.strListORHistroyORSpam=@"Spam";
        [self performSegueWithIdentifier:@"myList" sender:self];
        
    }
    else if ([[dic valueForKey:@"name"] isEqualToString:@"Cards"]){
        CardsVC *cards = [self.storyboard instantiateViewControllerWithIdentifier:@"CardsVC"];
        cards.title = @"Cards";
        [self.navigationController pushViewController:cards animated:YES];
    }
    else
    {
        appDelegate.strCategroryID=[dic valueForKey:@"id"];
        appDelegate.strCategoryName=[dic valueForKey:@"name"];
        [self performSegueWithIdentifier:@"Sub_Menu" sender:self];
    }

    
}
- (UITableViewCell*)createCellWithTitle:(NSString *)title image:(NSString *)image  indexPath:(NSIndexPath*)indexPath
{
    NSString *CellIdentifier = @"Cell";
    ExpandableTableViewCell* cell = [self.menuTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = bgView;
    cell.backgroundColor=[UIColor clearColor];
    cell.lblTitle.text = title;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.imgIcon.image=[UIImage imageNamed:image];
    
    [cell setIndentationLevel:[[[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    NSDictionary *d1=[arrUserMenuList objectAtIndex:indexPath.row] ;
    
    if([d1 valueForKey:@"SubMenu"])
    {
        cell.btnExpand.alpha = 1.0;
        [cell.btnExpand addTarget:self action:@selector(showSubMenu:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.btnExpand.alpha = 0.0;
    }
    return cell;
}

-(void)showSubMenu :(id) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.menuTableView];
    NSIndexPath *indexPath = [self.menuTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
//    if(btn.alpha==1.0)
//    {
//        if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"down-arrow.png"]])
//        {
//            [btn setImage:[UIImage imageNamed:@"up-arrow.png"] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [btn setImage:[UIImage imageNamed:@"down-arrow.png"] forState:UIControlStateNormal];
//        }
//        
//    }
    
    NSDictionary *d=[arrUserMenuList objectAtIndex:indexPath.row] ;
    NSArray *arr=[d valueForKey:@"SubMenu"];
    if([d valueForKey:@"SubMenu"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *SubMenu in arr )
        {
            NSInteger index=[arrUserMenuList indexOfObjectIdenticalTo:SubMenu];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [arrUserMenuList insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    
    
}
-(void)CollapseRows:(NSArray*)ar
{
    for(NSDictionary *dInner in ar )
    {
        NSUInteger indexToRemove=[arrUserMenuList indexOfObjectIdenticalTo:dInner];
        NSArray *arInner=[dInner valueForKey:@"SubMenu"];
        if(arInner && [arInner count]>0)
        {
            [self CollapseRows:arInner];
        }
        
        if([arrUserMenuList indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
            [arrUserMenuList removeObjectIdenticalTo:dInner];
            [self.menuTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
}



@end
