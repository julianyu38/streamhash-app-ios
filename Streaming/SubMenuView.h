//
//  SubMenuView.h
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMenuView : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *SideBarButton;

@end
