//
//  SubMenuView.m
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "SubMenuView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import "viewAllViewController.h"

@interface SubMenuView ()<WebServiceDelegate>
{
    AppDelegate *appDelgate;
    NSArray *arrList;
    UIScrollView *scrollView;
    NSDictionary *dictSendVal;
    NSString *strAd_VideoID;
    NSArray *arrViewAll;
    
    NSString *strViewTitle;
    
    
     NSString *strSendKey,*strTotalCount,*strIdenty;
    
}

@end

@implementation SubMenuView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelgate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title=appDelgate.strCategoryName;
    strIdenty=@"category";
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
   [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"category_id\":\"%@\"}",strID,strToken,appDelgate.strCategroryID];
    
    [service executeWebserviceWithMethod:METHOD_GET_VIDEO withValues:strSend];


}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
}
-(void)onPageLoad
{
    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.showsHorizontalScrollIndicator=NO;
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    
    int yPos=10;
    for (int nCount=0; nCount<arrList.count; nCount++)
    {
        int xPos=5;
        
        NSDictionary *dictLocal=[arrList objectAtIndex:nCount];
        NSArray *arrLocal=[dictLocal valueForKey:@"videos"];
        if (arrLocal.count!=0)
        {
            UILabel *lblNameTile=[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width-40, 25)];
            lblNameTile.text=[dictLocal valueForKey:@"sub_category_name"];
            [lblNameTile setFont:[UIFont boldSystemFontOfSize:15]];
            lblNameTile.textAlignment=NSTextAlignmentLeft;
            lblNameTile.textColor=[UIColor whiteColor];
            [scrollView addSubview:lblNameTile];
            
            UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width
                                                                        -110, yPos, 100, 20)];
            [btnMore setTitle:[dictLocal valueForKey:@"key"] forState:UIControlStateFocused];
            [btnMore setTitle:[dictLocal valueForKey:@"sub_category_name"] forState:UIControlStateHighlighted];
            [btnMore setTitle:@"See All >" forState:UIControlStateNormal];
            [btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
           // [btnMore setTitleColor:[UIColor colorWithRed:218.0f/255.0f green:106.0f/255.0f blue:44.0f/255.0f alpha:1] forState:UIControlStateNormal];
            [btnMore.titleLabel setFont:[UIFont systemFontOfSize:13]];
            btnMore.contentHorizontalAlignment=NSTextAlignmentRight;
            // btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
            [btnMore addTarget:self action:@selector(onViewAll:) forControlEvents:UIControlEventTouchDown];
            // [btnMore setBackgroundImage:[UIImage imageNamed:@"view_more.png"] forState:UIControlStateNormal];
            [scrollView addSubview:btnMore];
            
            yPos+=30;
            
            UIScrollView *scrollViewSubView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width,150)];
            scrollViewSubView.showsHorizontalScrollIndicator=NO;
            scrollViewSubView.showsVerticalScrollIndicator=NO;
            [scrollView addSubview:scrollViewSubView];
            
            for (int i=0; i<arrLocal.count; i++)
            {
                NSDictionary *dictLoc=[arrLocal objectAtIndex:i];
                
                UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0,115,140)];
                viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
                viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
                viewContent.layer.shadowOpacity = 0.7f;
                viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
                viewContent.layer.shadowRadius = 1.0f;
                viewContent.layer.cornerRadius = 2.0f;
                viewContent.tag=123;
                [scrollViewSubView addSubview:viewContent];
                
                UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,110)];
                imageView.layer.cornerRadius = 2.0f;
                [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                [viewContent addSubview:imageView];
                
                UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
                imgPlay.image=[UIImage imageNamed:@"play.png"];
                //imgPlay.alpha=0.6;
                [viewContent addSubview:imgPlay];
                
                UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
                btnClick.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
                //[btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
                [viewContent addSubview:btnClick];
                
                //Single tap
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
                tapGesture.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
                tapGesture.numberOfTapsRequired = 1;
                tapGesture.numberOfTouchesRequired = 1;
                [btnClick addGestureRecognizer:tapGesture];
                
                //Long press
//                UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleAdd:)];
//                longPress.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//                longPress.minimumPressDuration = 1.5;
//                [btnClick addGestureRecognizer:longPress];
                
                
                UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 113, viewContent.frame.size.width-10, 20)];
                lblTitle.text=[dictLoc valueForKey:@"title"];
                [lblTitle setFont:[UIFont systemFontOfSize:12]];
                lblTitle.textColor=[UIColor whiteColor];
                lblTitle.lineBreakMode = NSLineBreakByClipping;
                [viewContent addSubview:lblTitle];
                
                
//                UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 120, viewContent.frame.size.width-10, 20)];
//                lblDate.text=[dictLoc valueForKey:@"publish_time"];
//                [lblDate setFont:[UIFont systemFontOfSize:12]];
//                lblDate.textColor=[UIColor grayColor];
//                [viewContent addSubview:lblDate];

                
                
//                UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(viewContent.frame.size.width-25, viewContent.frame.size.height-25, 25, 25)];
//                btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//                [btnMore addTarget:self action:@selector(onSingleAdd:) forControlEvents:UIControlEventTouchDown];
//                [btnMore setBackgroundImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
//                [viewContent addSubview:btnMore];

                
                xPos+=120;
            }
            scrollViewSubView.contentSize=CGSizeMake(xPos, 150);
            yPos+=160;
        }
       
    }
     scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos);
    
}
-(IBAction)onViewAll:(id)sender
{
    UIButton *btn=(UIButton *) sender;
    
    strViewTitle=[btn titleForState:UIControlStateHighlighted];
    strSendKey=[btn titleForState:UIControlStateFocused];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=4;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"sub_category_id\":\"%@\",\"skip\":\"%@\"}",strID,strToken,[btn titleForState:UIControlStateFocused],@"0"];
    
    [service executeWebserviceWithMethod:METHOD_GET_SUB_CATEGORY withValues:strSend];
    
    
}
-(IBAction)onSingleAdd:(id)sender
{
    //UIButton *btn=(UIButton *) sender;
    
      UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=3;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
    
    
}
-(IBAction)onBtnClick:(id)sender
{
   // UIButton *btn=(UIButton *) sender;
   
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    
    strAd_VideoID=strVideoID;
    
    [self performSegueWithIdentifier:@"singleVideo" sender:self];
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.delegate=self;
//    service.tag=2;
//    
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    NSString *strID=[defaluts valueForKey:@"id"];
//    NSString *strToken=[defaluts valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
//    
//    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
   [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrList=[dictResponse valueForKey:@"data"];
            [self onPageLoad];
            
          
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle message:appDelgate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelgate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrViewAll=[dictResponse valueForKey:@"data"];
             strTotalCount=[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"total"]];
            if (arrViewAll.count !=0)
            {
                [self performSegueWithIdentifier:@"view_all" sender:self];
                
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle  message:@"No more details" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            //            dictSendVal=dictResponse;
            //            [self performSegueWithIdentifier:@"singleVideo" sender:self];
            
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle message:appDelgate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelgate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelgate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    }
//    else if (webservice.tag==2)
//    {
//        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
//        {
//            dictSendVal=dictResponse;
//            [self performSegueWithIdentifier:@"singleVideo" sender:self];
//            
//        }
//        else
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }
//    }
    
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
        singleVideo.strVideoID=strAd_VideoID;
    }
    else if ([[segue identifier] isEqualToString:@"view_all"])
    {
        viewAllViewController *view=[segue destinationViewController];
        view.arrList=arrViewAll;
        view.strTitleName=strViewTitle;
        view.strIdenty=strIdenty;
        view.strTotalCount=strTotalCount;
        view.strKey=strSendKey;
    }
}


@end
