//
//  ViewPlansVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "ViewPlansVC.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "VIewPlansCell.h"
#import "InvoiceVC.h"

@interface ViewPlansVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *plansArray;
}
@property (strong, nonatomic) IBOutlet UITableView *plansTableView;

@end

@implementation ViewPlansVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _plansTableView.delegate = self;
    _plansTableView.dataSource = self;
    
    self.plansTableView.rowHeight = UITableViewAutomaticDimension;
    self.plansTableView.estimatedRowHeight = 350;
    
    self.title = @"View Plans";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor ],NSFontAttributeName:[UIFont boldSystemFontOfSize:15]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22.0f/255.0f green:23.0f/255.0f blue:28.0f/255.0f alpha:1];
    
    plansArray = [NSMutableArray new];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:@"0" forKey:@"skip"];
    
    if (self.myPlans) {
        
        [self callService:METHOD_SUBSCRIBED_PLANS params:postDataDict key:@"CardList"];

    }else{
        [self callService:METHOD_VIEW_PLANS params:postDataDict key:@"CardList"];
    }
    
}

#pragma mark Tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return plansArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *planDetails = [plansArray objectAtIndex:indexPath.row];
    
    VIewPlansCell *cardCell = (VIewPlansCell *)[tableView dequeueReusableCellWithIdentifier:@"VIewPlansCell"];
    
    if (!cardCell) {
        
        cardCell = [[VIewPlansCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VIewPlansCell"];
    }
    
    if (self.myPlans) {
        
        cardCell.payButton.hidden = YES;
        cardCell.expireLabel.hidden = NO;
        cardCell.expireLabel.text =  [NSString stringWithFormat:@"Expires on %@",[planDetails objectForKey:@"expiry_date"]];
        
        if ([[planDetails objectForKey:@"popular_status"] integerValue] == 1) {
            cardCell.priceBackView.backgroundColor = [UIColor redColor];
        }else{
            cardCell.priceBackView.backgroundColor = [UIColor blackColor];
        }
    }
    
    else{
        
        if ([[planDetails objectForKey:@"popular_status"] integerValue] == 1) {
            cardCell.priceBackView.backgroundColor = [UIColor redColor];
        }else{
            cardCell.priceBackView.backgroundColor = [UIColor blackColor];
        }
        
        cardCell.payButton.hidden = NO;
        cardCell.expireLabel.hidden = YES;
    }
    
    cardCell.headingLabel.text = [planDetails objectForKey:@"title"];
    cardCell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[planDetails objectForKey:@"currency"],[planDetails objectForKey:@"amount"]];
    cardCell.contentLabel.text = [planDetails objectForKey:@"description"];
    cardCell.durationLabel.text = [NSString stringWithFormat:@"%@ months",[planDetails objectForKey:@"plan"]];
    cardCell.accountCountLabel.text = [NSString stringWithFormat:@"%@ Accounts",[planDetails objectForKey:@"no_of_account"]];
    
    cardCell.payButton.tag = indexPath.row;
    [cardCell.payButton addTarget:self action:@selector(payButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cardCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(VIewPlansCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    cell.backgroundColor = [UIColor colorWithRed:26/255 green:26/255 blue:26/255 alpha:1.0f];
}

- (void)payButtonTapped:(UIButton *)sender{
    
    InvoiceVC *invoice = [self.storyboard instantiateViewControllerWithIdentifier:@"InvoiceVC"];
    invoice.subscriptionDict = [plansArray objectAtIndex:sender.tag];
    invoice.backVC = self;
    [self presentViewController:invoice animated:YES completion:nil];
}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if ([dict objectForKey:@"success"]) {
                
                plansArray = [dict objectForKey:@"data"];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.plansTableView reloadData];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                
            });
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
