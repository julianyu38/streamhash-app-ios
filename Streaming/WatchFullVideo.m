//
//  WatchFullVideo.m
//  Streaming
//
//  Created by KrishnaDev on 10/10/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "WatchFullVideo.h"
#import <JWPlayer-iOS-SDK/JWPlayerController.h>
#import "YTPlayerView.h"
#import "AppDelegate.h"
@interface WatchFullVideo ()<YTPlayerViewDelegate,JWPlayerDelegate,UIGestureRecognizerDelegate>
{
    YTPlayerView *playerView;
    NSTimer *timerHide;
    AppDelegate *appDelegate;
    
    UIView *viewHeader;
}
@property (nonatomic) JWPlayerController *player;
@end

@implementation WatchFullVideo
@synthesize strUserType,strVideoURL,strTitle,strImageURL;


- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
  
    appDelegate.strPlayerIdenty=@"yes";
    [appDelegate setShouldRotate:YES];
   
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
   // self.navigationController.navigationBar.translucent =NO;

     self.navigationController.navigationBar.hidden=YES;
     [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
//     [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
    
    // Do any additional setup after loading the view.
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(windowNowVisible:)
//     name:UIWindowDidBecomeVisibleNotification
//     object:self.view.window
//     ];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeStarted:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeFinished:) name:@"UIMoviePlayerControllerWillExitFullscreenNotification" object:nil];
    
//    [timerHide invalidate];
//    timerHide=nil;
//    
//    timerHide=[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    
//    // [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
//    
//    self.navigationController.navigationBar.translucent =YES;
//    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    
//    
// 
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.hidden=NO;
 //   [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
   
    
    
   // [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 0)];
    
     [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    //  [self.navigationController.navigationBar setFrame:CGRectMake(0, 10, self.view.frame.size.width , 50)];
    

    
    if ([strUserType isEqualToString:@"2"])
    {
        playerView=[[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        playerView.backgroundColor=[UIColor blackColor];
        [self.view addSubview:playerView];
        
        playerView.delegate = self;
        
       
        
        NSArray *arrLocal=[strVideoURL componentsSeparatedByString:@"/"];
        
        NSString *strVideo=[arrLocal lastObject];
        
        NSDictionary *playerVars = @{
                                     @"controls" : @2,
                                     @"autoplay" :@1,
                                     @"playsinline" : @1,
                                    @"autohide" : @1,
                                     @"theme" : @"light",
                                     @"color" : @"red"
                                     // @"showinfo" : @0,
                                    // @"rel" : @0,
                                     
                                     };
        [playerView loadWithVideoId:strVideo playerVars:playerVars];
        
      
        
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayerTapped:)];
        singleFingerTap.numberOfTapsRequired = 1;
        singleFingerTap.delegate = self;
        [playerView addGestureRecognizer:singleFingerTap];
        
          playerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;

    }
    else
    {
        JWConfig *config = [[JWConfig alloc] init];
        
        config=[JWConfig configWithContentURL:strVideoURL];
        
        _player = [[JWPlayerController alloc] initWithConfig:config];
        config.size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
        _player.view.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:_player.view];
        
        config.image =strImageURL;;           //title image
        config.title = strTitle;        // video title
        config.controls = YES;  //default       //show/hide controls
        config.repeat = NO;   //default     //repeat after completion
        config.premiumSkin = JWPremiumSkinStormtrooper;    //JW premium skin
        config.cssSkin = @"http://p.jwpcdn.com/iOS/Skins/nature01/nature01.css";
        
        config.autostart = YES;
        
        self.player.delegate=self;
        
      _player.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
//        
        _player.openSafariOnAdClick = YES;
     //   _player.forceFullScreenOnLandscape = YES;
      //  _player.forceLandscapeOnFullScreen = YES;
    
    }
    
   // [self shouldAutorotate];
    viewHeader=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    viewHeader.backgroundColor=[UIColor clearColor];
    [self.view addSubview:viewHeader];
    
    UIButton *btnBack=[[UIButton alloc]initWithFrame:CGRectMake(20, 20, 30, 30)];
    [btnBack addTarget:self action:@selector(onBack:) forControlEvents:UIControlEventTouchDown];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [viewHeader addSubview:btnBack];
    
    viewHeader.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    

    
}
//- (BOOL)shouldAutorotate {
//    return YES;
//}
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskAll;
//}
-(void)viewWillAppear:(BOOL)animated
{
    
   
}
-(void)HideNavBar
{
    viewHeader.hidden=YES;
    // hide the Navigation Bar
    // [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)onDisplayClick
{
    //NSLog(@"clicked");
    
    if (viewHeader.hidden == NO)
    {
        viewHeader.hidden=YES;
        // hide the Navigation Bar
        //  [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    // if Navigation Bar is already hidden
    else if (viewHeader.hidden == YES)
    {
        // Show the Navigation Bar
        
        viewHeader.hidden=NO;
        
        //    [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        [timerHide invalidate];
        timerHide=nil;
        
        timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
        
    }
    
}

//Delegate Method

-(void) onPlayerTapped:(UIGestureRecognizer *)gestureRecognizer {
    // isInPlayingMode = NO;
    //NSLog(@"clicked");
    [self onDisplayClick];
}
#pragma mark youtube delegate methods

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification {
    if([notification.name isEqual:@"Playback started"] && notification.object != self) {
        [playerView pauseVideo];
    }
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            //NSLog(@"Started playback");
            break;
        case kYTPlayerStatePaused:
            //NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}

- (void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality{
    
}
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error{
    
}
#pragma mark -- Jw Player Delegate
#pragma mark - callback delegate methods

-(void)onTime:(double)position ofDuration:(double)duration
{
    NSString *playbackPosition = [NSString stringWithFormat:@"%.01f/.01%f", position, duration];
    // self.playbackTime.text = playbackPosition;
}
-(void)onPlay
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onPause
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onBuffer
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onIdle
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onReady
{
    //  [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onFirstFrame:(NSInteger)loadTime
{
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}
-(void)onComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdSkipped:(NSString *)tag
{
    //  [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdComplete:(NSString *)tag
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdImpression:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforePlay
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforeComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPlay:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPause:(NSString *)tag
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onAdError:(NSError *)error
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}



#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [[UIDevice currentDevice] setValue:
//     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
//                                forKey:@"orientation"];
//
    [playerView removeFromSuperview];
    [_player.view removeFromSuperview];
    [timerHide invalidate];
    timerHide=nil;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    // [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    self.navigationController.navigationBar.translucent =YES;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
   // [self.navigationController.navigationBar sizeToFit];

//    self.navigationController.navigationBar.translucent = NO;
//    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    
//    self.navigationController.navigationBar.hidden=NO;
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
  //    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
//-(void) youTubeStarted:(NSNotification*) notif {
//    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    appDelegate.fullScreenVideoIsPlaying = YES;
//}
//-(void) youTubeFinished:(NSNotification*) notif {
//    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    appDelegate.fullScreenVideoIsPlaying = NO;
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender {


    [self.navigationController popViewControllerAnimated:YES];
}
@end
