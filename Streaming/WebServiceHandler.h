//
//  WebServiceHandler.h
//  WebServiceDemo
//
//  Created by Dhanasekaran on 05/11/17.
//  Copyright © 2017 Premkumar. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define HOST @"https://cawordpressdemos.com"
#define API @""

//Service URL

#define HOST @"http://adminview.streamhash.com/" //@"http://default.startstreaming.info/"

//Methods

#define METHOD_REGISTER @"userApi/register"
#define METHOD_LOGIN @"userApi/login"
#define METHOD_FORGOT_PASSWORD @"userApi/forgotpassword"
#define METHOD_CHANGE_PASSWORD @"userApi/changePassword"
#define METHOD_GET_USER_PROFILE @"userApi/userDetails"
#define METHOD_PROFILE_UPDATE @"userApi/updateProfile"
#define METHOD_TOKEN_RENEW @"userApi/tokenRenew"
#define METHOD_PAYPAL @"userApi/paybypaypal"
#define METHOD_GET_CATEGORIES @"userApi/categories"
#define METHOD_GET_SUB_CATEGORIES @"userApi/subCategories"
#define METHOD_HOME @"userApi/home"
#define METHOD_GET_VIDEO @"userApi/categoryVideos"
#define METHOD_GET_SUB_CATEGORY @"userApi/subCategoryVideos"
#define METHOD_SEARCH_VIDEO @"userApi/searchVideo"
#define METHOD_SINGLE_VIDEO @"userApi/singleVideo"
#define METHOD_USER_RATING @"userApi/userRating"
#define METHOD_ADD_WISHLIST @"userApi/addWishlist"
#define METHOD_GET_WISHLIST @"userApi/getWishlist"
#define METHOD_DELETE_LIST @"userApi/deleteWishlist"

#define METHOD_ADD_HISTORY @"userApi/addHistory"
#define METHOD_GET_HISTROY @"userApi/getHistory"
#define METHOD_DELETE_HISTORY @"userApi/deleteHistory"
#define METHD_CLEAR_HISTORY @"userApi/deleteHistory"
#define METHOD_VIEW_ALL @"userApi/common"

#define METHOD_LIKE_VIDEO @"userApi/like_video"
#define METHOD_DISLIKE_VIDEO @"userApi/dis_like_video"

#define METHOD_REPORTSPAMLIST @"userApi/spam-reasons"
#define METHOD_ADDSPAM @"userApi/add_spam"
#define METHOD_SPAMVIDEOSLIST @"userApi/spam_videos"
#define METHOD_REMOVE_SPAM @"userApi/remove_spam"

#define METHOD_MANAGE_PROFILES @"userApi/active-profiles"
#define METHOD_ADD_PROFILE @"userApi/add-profile"
#define METHOD_DELETE_SUBPROFILE @"userApi/delete-sub-profile"

#define METHOD_CARD_LIST @"userApi/card_details"
#define METHOD_DELETE_CARD @"userApi/delete_card"

#define METHOD_PRIVACY @"privacy"
#define METHOD_NOTIFICATION @"userApi/settings"
#define METHOD_DELETE @"userApi/deleteAccount"
#define METHOD_PLAN_LISTS @"userApi/subscription_plans"
#define METHOD_PAID_VIDEOS @"userApi/ppv_list"

#define METHOD_VIEW_PLANS  @"userApi/subscription_plans"

#define METHOD_SUBSCRIBED_PLANS @"userApi/subscribedPlans"

#define METHOD_ADD_CREDIT_CARD @"userApi/payment_card_add"

#define METHOD_ADD_DEFAULT_CARD @"userApi/default_card"

#define METHOD_STRIPE_PAY_PLAN @"userApi/stripe_payment"

typedef enum : NSUInteger {
    GET = 0,
    POST,
    PUT,
    DELETE
} WebMethod;

typedef void(^completionHandler)(NSDictionary *dict, NSURLResponse *response, NSError *error);

@interface WebServiceHandler : NSObject

+ (WebServiceHandler *)sharedInstance;

- (void)sendRequest:(NSString *)service isApiRequired:(BOOL)isApiRequired  urlParameters:(NSDictionary *)params method:(WebMethod)method isAuthRequired:(BOOL)isAuthRequired postData:(NSData *)postData withCompletionHandler:(completionHandler)completionHandler;


@end
