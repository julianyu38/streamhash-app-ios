//
//  WebServiceHandler.m
//  WebServiceDemo
//
//  Created by Dhanasekaran on 05/11/17.
//  Copyright © 2017 Premkumar. All rights reserved.
//

#import "WebServiceHandler.h"

@implementation WebServiceHandler

+ (WebServiceHandler *)sharedInstance {
    static WebServiceHandler *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (void)sendRequest:(NSString *)service isApiRequired:(BOOL)isApiRequired  urlParameters:(NSDictionary *)params method:(WebMethod)method isAuthRequired:(BOOL)isAuthRequired postData:(NSData *)postData withCompletionHandler:(completionHandler)completionHandler {
    /*** URL String***/
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@",HOST];
    if (isApiRequired) {
        [urlString appendString:API];
    }
    
    if (service && ![service isEqualToString:@""]) {
        [urlString appendString:service];
    }
    
    if (params.count) {
        NSMutableArray *paramArr = [[NSMutableArray alloc] init];
        for (NSString *key in params) {
            [paramArr addObject:[NSString stringWithFormat:@"%@=%@",key,[params objectForKey:key]]];
        }
        
        [urlString appendString:[NSString stringWithFormat:@"?%@",[paramArr componentsJoinedByString:@"&"]]];
        
    }
    NSLog(@"%@",urlString);
    
    /**** URL ***/
    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]].mutableCopy;
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"URL %@",url);
    
    /*** Request ***/
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    switch (method) {
        case GET:
            [request setHTTPMethod:@"GET"];
            break;
        case POST:
            [request setHTTPMethod:@"POST"];
            break;
        case DELETE:
            [request setHTTPMethod:@"DELETE"];
            break;
        case PUT:
            [request setHTTPMethod:@"PUT"];
            break;
            
        default:
            break;
    }
    
    /***Post Data***/
    if (!postData) {
        NSError *error1;
        
        NSDictionary *emptyDict = [[NSDictionary alloc] init];
        postData = [NSJSONSerialization dataWithJSONObject:emptyDict options:NSJSONWritingPrettyPrinted error:&error1];
        
        
    }
    
    if (method == POST || method == DELETE || method == PUT) {
        [request setHTTPBody:postData];
        [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"text/html; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"multipart/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
//        [request addValue:@"charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    }
    
    if (isAuthRequired) {
        //window-- >nt
        //        NSURLCredential *credential = [NSURLCredential credentialWithUser:@"ssmrt1000"
        //                                                                 password:@"1!abcdef"
        //                                                              persistence:NSURLCredentialPersistenceForSession];
        //        [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:[serverContext urlCredentials]
        //                                                            forProtectionSpace:[serverContext protectionSpace]];
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"username", @"password"];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    }
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *responseDict =  [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",responseDict);
        
        completionHandler(responseDict, response, error);
    
    }];
    [dataTask resume];
}

@end
