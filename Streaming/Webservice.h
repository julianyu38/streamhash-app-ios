//
//  Webservice.h
//  Royal Water
//
//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

//Service URL
//#define SERVICE_URL @"http://startstreaming.co/"
//#define SERVICE_URL @"http://default.streamhash.com/" //@"http://default.startstreaming.info/"

#define SERVICE_URL @"http://adminview.streamhash.com/" //@"http://default.startstreaming.info/"



//Methods       

#define METHOD_REGISTER @"userApi/register"
#define METHOD_LOGIN @"userApi/login"
#define METHOD_FORGOT_PASSWORD @"userApi/forgotpassword"
#define METHOD_CHANGE_PASSWORD @"userApi/changePassword"
#define METHOD_GET_USER_PROFILE @"userApi/userDetails"
#define METHOD_PROFILE_UPDATE @"userApi/updateProfile"
#define METHOD_TOKEN_RENEW @"userApi/tokenRenew"
#define METHOD_PAYPAL @"userApi/paybypaypal"
#define METHOD_GET_CATEGORIES @"userApi/categories"
#define METHOD_GET_SUB_CATEGORIES @"userApi/subCategories"
#define METHOD_HOME @"userApi/home"
#define METHOD_GET_VIDEO @"userApi/categoryVideos"
#define METHOD_GET_SUB_CATEGORY @"userApi/subCategoryVideos"
#define METHOD_SEARCH_VIDEO @"userApi/searchVideo"
#define METHOD_SINGLE_VIDEO @"userApi/singleVideo"
#define METHOD_USER_RATING @"userApi/userRating"
#define METHOD_ADD_WISHLIST @"userApi/addWishlist"
#define METHOD_GET_WISHLIST @"userApi/getWishlist"
#define METHOD_DELETE_LIST @"userApi/deleteWishlist"
#define METHOD_ADD_HISTORY @"userApi/addHistory"
#define METHOD_GET_HISTROY @"userApi/getHistory"
#define METHOD_DELETE_HISTORY @"userApi/deleteHistory"
#define METHD_CLEAR_HISTORY @"userApi/deleteHistory"
#define METHOD_VIEW_ALL @"userApi/common"

#define METHOD_PRIVACY @"privacy"
#define METHOD_TERMS @"terms_condition"

#define METHOD_NOTIFICATION @"userApi/settings"
#define METHOD_DELETE @"userApi/deleteAccount"

#define METHOD_LIKE_VIDEO @"userApi/like_video"
#define METHOD_DISLIKE_VIDEO @"userApi/dis_like_video"




@class Webservice;

@protocol WebServiceDelegate <NSObject,NSURLSessionDelegate,NSURLSessionDataDelegate>

-(void) receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice;
-(void) receivedErrorWithMessage:(NSString *)message;

@end

@interface Webservice : NSObject
{
    NSMutableData *receivedData;
}

@property(nonatomic,retain) id<WebServiceDelegate> delegate;
@property int tag;

- (void) executeWebserviceWithMethod:(NSString *) method withValues:(NSString *) values;
-(void) executeWebserviceWithMethodinImage:(NSString *)method withValues:(NSURLRequest *)values ;
//- (void) executeWebserviceWithGetMethod:(NSString *) method;
- (void) executeWebserviceWithMethod1:(NSString *) method withValues:(NSString *) values;

@end
