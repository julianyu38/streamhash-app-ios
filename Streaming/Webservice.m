//
//  Webservice.m
//  //

//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import "Webservice.h"

@implementation Webservice
@synthesize tag;

-(void)executeWebserviceWithMethod:(NSString *)method withValues:(NSString *)values
{
    //NSLog(@"REQ: %@",values);
    
 /*   NSData *postData = [values dataUsingEncoding:NSUTF8StringEncoding];
    NSString *strURL = [SERVICE_URL stringByAppendingString:method];
    // NSString *strURL = [SERVICE_URL stringByAppendingFormat:@"%@%@",method,values];
    NSURL *requestURL = [NSURL URLWithString:strURL];
    
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:requestURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:300];
    
    [req setHTTPMethod:@"POST"];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [req setValue:postLength  forHTTPHeaderField:@"Content-Length"];
    
    
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setHTTPBody:postData];
    
     
    //NSLog(@"%@",req);
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    
    if(conn)
    {
        receivedData = [[NSMutableData alloc] init];
        [conn start];
    }
    else
    {
        [self.delegate receivedErrorWithMessage:@"Cannot create connection."];
    }

    */
    
    
/*   NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"[JSON SERVER"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    
    
    NSData *data = [values dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
   // NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
                          //   @"IOS TYPE", @"typemap",
                           //  nil];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        
    }];
    
    [postDataTask resume]; */
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    
    NSString *strURL = [SERVICE_URL stringByAppendingString:method];
    
   // NSURL *requestURL = [NSURL URLWithString:strURL];

    
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
   // NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:[values dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
    [dataTask resume];
   
    
}
-(void) executeWebserviceWithMethodinImage:(NSString *)method withValues:(NSURLRequest *)values
{
 /*   NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:values delegate:self];
    
    if(conn)
    {
        receivedData = [[NSMutableData alloc] init];
        [conn start];
    }
    else
    {
        [self.delegate receivedErrorWithMessage:@"Cannot create connection."];
    }
    */
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:values];
    [dataTask resume];
    
  
    
}

- (void) executeWebserviceWithMethod1:(NSString *) method withValues:(NSString *) values
{
    
    NSString *strURL =[NSString stringWithFormat:@"%@%@%@",SERVICE_URL,method,values];
  //  NSURL *requestURL = [NSURL URLWithString:strURL];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
      
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    // NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
   // [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  //  [urlRequest setHTTPBody:[values dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
    [dataTask resume];

    
    
  /*  NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:requestURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:300];
    
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    
    if(conn)
    {
        receivedData = [[NSMutableData alloc] init];
        [conn start];
    }
    else
    {
        [self.delegate receivedErrorWithMessage:@"Cannot create connection."];
    }*/
    
}

/*- (void) executeWebserviceWithGetMethod:(NSString *) method;
{
    NSString *strURL =[NSString stringWithFormat:@"%@%@",SERVICE_URL,method];
    NSURL *requestURL = [NSURL URLWithString:strURL];
    
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:requestURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:300];
    
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    
    if(conn)
    {
        receivedData = [[NSMutableData alloc] init];
        [conn start];
    }
    else
    {
        [self.delegate receivedErrorWithMessage:@"Cannot create connection."];
        
    }
    
    
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate receivedErrorWithMessage:[error localizedDescription]];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
       NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //    NSData* data = [response dataUsingEncoding:NSUTF8StringEncoding];
    //    NSString *json_string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //    SBJSON *parser = [[SBJSON alloc] init];
    //    NSDictionary *values = [parser objectWithString:json_string error:nil];
    
    NSError *error;
    NSDictionary *values = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:&error];
    //NSLog(@"%@", values);
    
    if(values)
    {
        if([self.delegate respondsToSelector:@selector(receivedResponse:fromWebservice:)])
        {
            [self.delegate receivedResponse:values fromWebservice:self];
        }
        else
        {
            [self.delegate receivedErrorWithMessage:[values valueForKey:@"Error"]];
        }
    }
    else
    {
        [self.delegate receivedErrorWithMessage:@"No response / Invalid response from server."];
    }
}
*/


- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    
    receivedData=nil; receivedData=[[NSMutableData alloc] init];
    [receivedData setLength:0];
    
    completionHandler(NSURLSessionResponseAllow);
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    
    [receivedData appendData:data];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error {
    
    if (error) {
        // Handle error
      //   [self.delegate receivedErrorWithMessage:@"No response / Invalid response from server."];
          [self.delegate receivedErrorWithMessage:@"please check your internet connection"];
    }
    else {
        NSDictionary* response=(NSDictionary*)[NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];
        // perform operations for the  NSDictionary response
        
        if(response)
        {
            if([self.delegate respondsToSelector:@selector(receivedResponse:fromWebservice:)])
            {
                [self.delegate receivedResponse:response fromWebservice:self];
            }
            else
            {
                [self.delegate receivedErrorWithMessage:[response valueForKey:@"Error"]];
            }
        }
        else
        {
            [self.delegate receivedErrorWithMessage:@"No response / Invalid response from server."];
        }
        
        //NSLog(@"%@",response );
    }
}


@end
