//
//  viewAllViewController.h
//  Streaming
//
//  Created by KrishnaDev on 05/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface viewAllViewController : UIViewController


@property(strong,nonatomic) NSArray *arrList;
@property(strong,nonatomic) NSString *strTitleName;

@property(strong,nonatomic) NSString *strKey,*strTotalCount,*strIdenty;

@end
