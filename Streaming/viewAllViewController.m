//
//  viewAllViewController.m
//  Streaming
//
//  Created by KrishnaDev on 05/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "viewAllViewController.h"
#import "SingleVideoView.h"
#import "UIImageView+WebCache.h"
#import "Webservice.h"
#import "ProgressIndicator.h"
#import "AppDelegate.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>


@interface viewAllViewController ()<WebServiceDelegate,UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UIScrollView *scrollView;
    NSString *strAd_VideoID;
    AppDelegate *appDelegate;
    
    NSMutableArray *arrAllList;
    
    UIRefreshControl *refreshControl;
    
    int nSendSkipVal;
    
     UICollectionView *_collectionView;
}

@end

@implementation viewAllViewController
@synthesize arrList,strTitleName;

@synthesize strKey,strTotalCount,strIdenty;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=strTitleName;
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    nSendSkipVal=12;
    
    arrAllList=[[NSMutableArray alloc]init];
    arrAllList =[NSMutableArray arrayWithArray:arrList];
    
//    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    scrollView.delegate=self;
//    scrollView.showsHorizontalScrollIndicator=NO;
//    scrollView.showsVerticalScrollIndicator=NO;
//   
//    
//    refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
//    [scrollView addSubview:refreshControl];
//  //
//  //  [scrollView insertSubview:refreshControl atIndex:0];
//    
//     [self.view addSubview:scrollView];
//    
//    [self onPageLoad];
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-60) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
    
//    refreshControl = [[UIRefreshControl alloc] init];
//    refreshControl.tintColor = [UIColor whiteColor];
//    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
//    [_collectionView addSubview:refreshControl];
    
    _collectionView.alwaysBounceVertical=YES;
    _collectionView.showsVerticalScrollIndicator=NO;
    _collectionView.showsHorizontalScrollIndicator=NO;
    
    refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(Refresh:) forControlEvents:UIControlEventValueChanged];
    _collectionView.bottomRefreshControl = refreshControl;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrAllList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    //cell.backgroundColor=[UIColor greenColor];
    
     NSDictionary *dictLocal=[arrAllList objectAtIndex:indexPath.row];
    
    [[cell viewWithTag:123] removeFromSuperview];   // REMOVING PREVIOUS CONTENT VIEW
    
    UIView *viewContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
    viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewContent.layer.shadowOpacity = 0.7f;
    viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    viewContent.layer.shadowRadius = 1.0f;
    viewContent.layer.cornerRadius = 2.0f;
    viewContent.tag=123;
    [cell addSubview:viewContent];
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,110)];
    imageView.layer.cornerRadius = 2.0f;
    [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [viewContent addSubview:imageView];
    
    UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
    imgPlay.image=[UIImage imageNamed:@"play.png"];
    //imgPlay.alpha=0.6;
    [viewContent addSubview:imgPlay];
    
    UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
    btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    //[btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
    [viewContent addSubview:btnClick];
    
    //Single tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
    tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [btnClick addGestureRecognizer:tapGesture];
    
    //Long press
    //            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClear:)];
    //            longPress.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    //            longPress.minimumPressDuration = 1.5;
    //            [btnClick addGestureRecognizer:longPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 113, viewContent.frame.size.width-10, 20)];
    
    lblTitle.text=[dictLocal valueForKey:@"title"];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
    lblTitle.textColor=[UIColor whiteColor];
    lblTitle.lineBreakMode = NSLineBreakByClipping;
    [viewContent addSubview:lblTitle];

    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3-10, 140);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)Refresh:(UIRefreshControl *)refreshControl1
{
//     [refreshControl endRefreshing];
    
    int nVal=[strTotalCount intValue];
    
    if (nVal>nSendSkipVal)
    {
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=4;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        
        if ([strIdenty isEqualToString:@"home"])
        {
            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"key\":\"%@\",\"skip\":\"%@\"}",strID,strToken,strKey,[NSString stringWithFormat:@"%d",nSendSkipVal]];
            
            [service executeWebserviceWithMethod:METHOD_VIEW_ALL withValues:strSend];
            
        }
        else
        {
            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"sub_category_id\":\"%@\",\"skip\":\"%d\"}",strID,strToken,strKey,nSendSkipVal];
            
            [service executeWebserviceWithMethod:METHOD_GET_SUB_CATEGORY withValues:strSend];
        }
        
        
        nSendSkipVal+=12;
    }
    else
         [refreshControl1 endRefreshing];
        
 
}


//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
//{
//    
//}
//
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView1
//{
//    
//   //  [refreshControl beginRefreshing];
//    
//    float scrollViewHeight = scrollView1.frame.size.height;
//    float scrollContentSizeHeight = scrollView1.contentSize.height;
//    float scrollOffset = scrollView1.contentOffset.y;
//    
//    if (scrollOffset == 0)
//    {
//        // then we are at the top
//    }
//    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
//    {
//        // then we are at the end
//     //   [self callWebService];
//        
//      //  [refreshControl beginRefreshing];
//     //   [arrAllList addObjectsFromArray:arrList];
//        //arrAllList=[NSMutableArray arrayWithArray:arrList];
//     //   [self onPageLoad];
//    }
//   
//}
//-(void)callWebService
//{
// 
//    
//    int nVal=[strTotalCount intValue];
//    
//    if (nVal>nSendSkipVal)
//    {
//        Webservice *service=[[Webservice alloc]init];
//        service.delegate=self;
//        service.tag=4;
//        
//        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//        NSString *strID=[defaluts valueForKey:@"id"];
//        NSString *strToken=[defaluts valueForKey:@"token"];
//        
//        
//        if ([strIdenty isEqualToString:@"home"])
//        {
//            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"key\":\"%@\",\"skip\":\"%@\"}",strID,strToken,strKey,[NSString stringWithFormat:@"%d",nSendSkipVal]];
//            
//            [service executeWebserviceWithMethod:METHOD_VIEW_ALL withValues:strSend];
//            
//        }
//        else
//        {
//            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"sub_category_id\":\"%@\",\"skip\":\"%d\"}",strID,strToken,strKey,nSendSkipVal];
//            
//            [service executeWebserviceWithMethod:METHOD_GET_SUB_CATEGORY withValues:strSend];
//        }
//      
//        
//        nSendSkipVal+=12;
//    }
//    
//    
//    
//   
//}
//
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    
//}

-(void)viewWillAppear:(BOOL)animated
{
//    [[UIDevice currentDevice] setValue:
//     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
//                                forKey:@"orientation"];
//     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
}
-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*-(void)onPageLoad
{
    /// [scrollView removeFromSuperview];
    
    
    for (UIView *v in scrollView.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
    int yPos=20;
    int xPos=10;
    int nVal=1;
    
    
    if (arrAllList.count!=0)
    {
//        UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, yPos, 100, 20)];
//        
//        lblTitle.text=@"History";
//        
//        [lblTitle setFont:[UIFont boldSystemFontOfSize:20]];
//        lblTitle.textColor=[UIColor grayColor];
//        [scrollView addSubview:lblTitle];
//        
//        yPos+=40;
        
        
        for (int nCount=1; nCount<=arrAllList.count; nCount++)
        {
            NSDictionary *dictLocal=[arrAllList objectAtIndex:nCount-1];
            
            UIView *viewContent;
            
            if (nVal==1)
            {
                xPos=5;
                nVal=2;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
            }
            else if (nVal==2)
            {
                xPos+=self.view.frame.size.width/3-3;
                nVal=3;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
            }
            else if (nVal==3)
            {
                xPos+=self.view.frame.size.width/3-3;
                viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos,self.view.frame.size.width/3-10,140)];
                xPos=13;
                yPos+=160;
                nVal=1;
            }
            
            
            viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
            viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
            viewContent.layer.shadowOpacity = 0.7f;
            viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
            viewContent.layer.shadowRadius = 1.0f;
            viewContent.layer.cornerRadius = 2.0f;
            viewContent.tag=123;
            [scrollView addSubview:viewContent];
            
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,110)];
            imageView.layer.cornerRadius = 2.0f;
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            [viewContent addSubview:imageView];
            
            UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
            imgPlay.image=[UIImage imageNamed:@"play.png"];
            //imgPlay.alpha=0.6;
            [viewContent addSubview:imgPlay];
            
            UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
            btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
            //[btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
            [viewContent addSubview:btnClick];
            
            //Single tap
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
            tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
            tapGesture.numberOfTapsRequired = 1;
            tapGesture.numberOfTouchesRequired = 1;
            [btnClick addGestureRecognizer:tapGesture];
            
            //Long press
//            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClear:)];
//            longPress.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
//            longPress.minimumPressDuration = 1.5;
//            [btnClick addGestureRecognizer:longPress];
            
            UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 113, viewContent.frame.size.width-10, 20)];
            
            lblTitle.text=[dictLocal valueForKey:@"title"];
            [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
            lblTitle.textColor=[UIColor whiteColor];
             lblTitle.lineBreakMode = NSLineBreakByClipping;
            [viewContent addSubview:lblTitle];
            
//            UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 123, viewContent.frame.size.width, 20)];
//            lblDate.text=[dictLocal valueForKey:@"publish_time"];
//            [lblDate setFont:[UIFont systemFontOfSize:12]];
//            lblDate.textColor=[UIColor grayColor];
//            [viewContent addSubview:lblDate];
            
//            UILabel *lblCategoryName=[[UILabel alloc]initWithFrame:CGRectMake(5, 130, viewContent.frame.size.width, 20)];
//            lblCategoryName.text=[dictLocal valueForKey:@"category_name"];
//            [lblCategoryName setFont:[UIFont boldSystemFontOfSize:13]];
//            lblCategoryName.textColor=[UIColor colorWithRed:230.0f/255.0f green:130.0f/255.0f blue:62.0f/255.0f alpha:1];
//            [viewContent addSubview:lblCategoryName];
//            
//            
//            UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(viewContent.frame.size.width-25, viewContent.frame.size.height-25, 25, 25)];
//            btnMore.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
//            [btnMore addTarget:self action:@selector(onSingleClear:) forControlEvents:UIControlEventTouchDown];
//            [btnMore setBackgroundImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
//            [viewContent addSubview:btnMore];
            
        }
        
    }
    else
    {
        yPos=150;
        
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, yPos, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [scrollView addSubview:imgeView];
        
        yPos+=110;
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops!  List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [scrollView addSubview:lblName];
        
        yPos+=35;
    }
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+200);
    
}*/
-(IBAction)onBtnClick:(id)sender
{
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
   // UIButton *btn=(UIButton *) sender;
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    strAd_VideoID=strVideoID;
     [self performSegueWithIdentifier:@"singleVideo" sender:self];
    
//    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//    
//    Webservice *service=[[Webservice alloc]init];
//    service.delegate=self;
//    service.tag=3;
//    
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    NSString *strID=[defaluts valueForKey:@"id"];
//    NSString *strToken=[defaluts valueForKey:@"token"];
//    
//    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
//    
//    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];
    
}
-(IBAction)onSingleClear:(id)sender
{
   // UIButton *btn=(UIButton *) sender;
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=1;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Stream Hash" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            //dictSendVal=dictResponse;
          //  [self performSegueWithIdentifier:@"singleVideo" sender:self];
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        }
    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [arrAllList addObjectsFromArray:[dictResponse valueForKey:@"data"]];
          //  [self onPageLoad];
            [_collectionView reloadData];
           // arrViewAll=[dictResponse valueForKey:@"data"];
           // strTotalCount=[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"total"]];
            
         //   if (arrViewAll.count !=0)
//            {
//                [self performSegueWithIdentifier:@"view_all" sender:self];
//                
//            }
//            else
//            {
//                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"No more details" preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alertController addAction:ok];
//                [self presentViewController:alertController animated:YES completion:nil];
//                
//            }
            //            dictSendVal=dictResponse;
            //            [self performSegueWithIdentifier:@"singleVideo" sender:self];
            
            
        }
        else
        {
            if ([[dictResponse valueForKey:@"error_code"] integerValue]== 104)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:appDelegate.strExpiredMsg preferredStyle:UIAlertControllerStyleAlert]; // 7
                
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                    [appDelegate onExpriedPage];
                    
                }]; // 8
                
                [alert addAction:defaultAction];
                
                
                
                [self presentViewController:alert animated:YES completion:nil]; // 11
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:[dictResponse valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
        singleVideo.strVideoID=strAd_VideoID;
    }

}


@end
